import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
//import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { UserPage } from '../pages/user/user';
import { UlistPage } from '../pages/ulist/ulist';
import { GalleryPage } from '../pages/gallery/gallery';
import { MyEventsPage } from '../pages/my-events/my-events';
import { EventListPage } from '../pages/event-list/event-list';
import { EventCategoryPage } from '../pages/event-category/event-category';
import { AttendancePage } from '../pages/attendance/attendance';
import { RequestPage } from '../pages/request/request';
import { FindUserPage } from '../pages/find-user/find-user';
import { MyGroupsPage } from '../pages/my-groups/my-groups';
import { MyWelfarePage } from '../pages/my-welfare/my-welfare';

import { UserService } from '../providers/user-service';

@Component({
  templateUrl: 'app.html',
})

export class MyApp {
  @ViewChild('content') nav: NavController;

  rootPage: any;

  userPage = UserPage;
  ulistPage = UlistPage;
  eventListPage = EventListPage;
  eventCategory = EventCategoryPage;
  attendance = AttendancePage;
  myEventsPage = MyEventsPage;
  myGroupsPage = MyGroupsPage;
  galleryPage = GalleryPage;
  findUserPage = FindUserPage;
  requestPage = RequestPage;
  myWelfarePage = MyWelfarePage;

  player: any;
  time: any;
  myWelfare:any = false;

  constructor(platform: Platform, public storage: Storage, private splashScreen: SplashScreen, private statusBar: StatusBar, public userService: UserService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      storage.get('logStatus').then((val)=>{
          if(val == true){
            console.log("loggedInAlready State !!");
            this.userService.loggedInAlready();
            
            setTimeout(()=>{
              if( this.userService.logStatus == true ){
                this.rootPage = UserPage;
                this.userService.getNotify();
              } else {
                this.userService.logStatus = "retry";
                this.rootPage = HomePage; 
                var time;
                time = setInterval(() =>{
                    if(this.userService.logStatus == true){
                      clearInterval(time);
                      this.rootPage = UserPage;
                      this.userService.getNotify();
                    }
                }, 700);   
              }
            },3000);
          } else {

            console.log("Login Require !!");
            this.rootPage = HomePage;
          
          }
      });
    });
    this.time = setInterval(() => {
      if (this.player != undefined) {
        clearInterval(this.time);
      }
      this.getPlayer();
    }, 3000);
  }

  getPlayer() {
    this.player = this.userService.getPlayer();
    console.log("PlayerData fetched!");
  }

  toggleMenu(){
    if(this.myWelfare == false  ){
      this.myWelfare = true;
    } else {
      this.myWelfare = false;
    }
  }

  logout(){
    this.userService.presentConfirm("logout");
    this.time = setInterval(() => {
      if (this.userService.logStatus == false) {
        clearInterval(this.time);
        this.openPage( HomePage ); 
      } else if( this.userService.logStatus == "error" ) {
        clearInterval(this.time);
      }
    }, 500);
  }

  openPage(p: any) {

    if( p == "ulistPage"){
      this.player = this.userService.getPlayer();
    }
    this.nav.setRoot(p, {
      player: this.player
    }).catch(error => {
      console.log(error);
    });

  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    
    this.userService.getNotify();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 700);
  }

  clearNotify(){
    this.storage.get("MaxNotifyId").then((val) =>{
      if( val != null ){
        var max = val;
      } else {
        var max: any = 0;
      }
      this.userService.notify.forEach(element => {
        if( element.nid > max ){
          max = element.nid;
        }
      });
      this.storage.set('MaxNotifyId',max);
      console.log("MAX: "+max);
      this.userService.notify = false;
      this.userService.setMaxNotifyId(max);
    });
  }
}
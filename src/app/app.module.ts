import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { IonicStorageModule } from '@ionic/storage';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Camera } from '@ionic-native/camera';
import { IonPullupModule } from 'ionic-pullup';
import { HttpModule } from '@angular/http';

import { UserService } from '../providers/user-service';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { UserPage } from '../pages/user/user';
import { UlistPage } from '../pages/ulist/ulist';
import { GalleryPage, SeeImage } from '../pages/gallery/gallery';
import { RequestPage, PopoverReqPage } from '../pages/request/request';
import { FindUserPage } from '../pages/find-user/find-user';
import { MyEventsPage } from '../pages/my-events/my-events';
import { EventListPage, PopoverPage } from '../pages/event-list/event-list';
import { EventCategoryPage } from '../pages/event-category/event-category';
import { AttendancePage, EventListMenu } from '../pages/attendance/attendance';
import { MyWelfarePage } from '../pages/my-welfare/my-welfare';
import { MyGroupsPage } from '../pages/my-groups/my-groups';
import { GroupInfoPage } from '../pages/group-info/group-info';
import { NewsfeedPage } from '../pages/newsfeed/newsfeed';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { UserInfoPage, MoreSettings } from '../pages/user-info/user-info';
import { CreateGroupPage } from '../pages/create-group/create-group';
import { EventDetailsPage, SeeeImage } from '../pages/event-details/event-details';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '0e53face'
  }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegisterPage,
    UserPage,
    UlistPage,
    MyEventsPage,
    EventListPage,
    PopoverPage,
    EventDetailsPage,
    SeeeImage,
    EventCategoryPage,
    AttendancePage,
    EventListMenu,
    MyWelfarePage,
    MyGroupsPage,
    CreateGroupPage,
    GroupInfoPage,
    NewsfeedPage,
    GalleryPage,
    SeeImage,
    FindUserPage,
    UserProfilePage,
    UserInfoPage,
    MoreSettings,
    RequestPage,
    PopoverReqPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CloudModule.forRoot(cloudSettings),
    IonPullupModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegisterPage,
    UserPage,
    UlistPage,
    MyEventsPage,
    EventListPage,
    PopoverPage,
    EventDetailsPage,
    SeeeImage,
    EventCategoryPage,
    AttendancePage,
    EventListMenu,
    MyWelfarePage,
    MyGroupsPage,
    CreateGroupPage,
    GroupInfoPage,
    NewsfeedPage,
    GalleryPage,
    SeeImage,
    FindUserPage,
    UserProfilePage,
    UserInfoPage,
    MoreSettings,
    RequestPage,
    PopoverReqPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},
              StatusBar, SplashScreen, MediaCapture, FileChooser, File, BarcodeScanner,
              Camera, ScreenOrientation, Storage, AndroidFingerprintAuth, UserService],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  
})
export class AppModule {}

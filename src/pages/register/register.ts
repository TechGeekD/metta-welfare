import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Injectable } from '@angular/core';
//import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { UserService } from '../../providers/user-service';

//import { HomePage } from "../home/home";

@Injectable()

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  http;

  uname: string;
  fullname: string;
  password: string;
  email: string;
  phone: number;
  statusx: string = "Active";
  regStatus: any;
  time: any;
  count: any = 1;

  constructor(public navCtrl: NavController, public userService: UserService) { }

  register() {
    this.userService.setPlayer({ uname: this.uname, fullname: this.fullname, passwordx: this.password, email: this.email, phone: this.phone, statusx: this.statusx });
    this.userService.presentConfirm(false);
    this.time = setInterval(() => {
      if (this.regStatus == true || this.regStatus == false) {
        clearInterval(this.time);
      }
      this.getRegStatus();
    }, 500);

  }
  
  getRegStatus() {
    this.regStatus = this.userService.getRegStatus()
    console.log("regStatus " + this.regStatus);

    if (this.regStatus == true) {
      console.log("In goToHome");
      this.goToHome();
      this.count += 1;
      console.log(this.count);
    } else {
      console.log("In goToHome ERROR");
    }
  }

  goToHome() {
    if(this.count == 1){
      this.navCtrl.pop(RegisterPage);
    }
  }

  ionViewDidLoad() {
    console.log('Hello RegisterPage Page');
  }

}

import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Slides } from 'ionic-angular';
import { IonPullUpFooterState } from 'ionic-pullup';

import { UserService } from '../../providers/user-service';

import { UserProfilePage } from '../user-profile/user-profile';

@Component({
  selector: 'page-event-details',
  templateUrl: 'event-details.html'
})
export class EventDetailsPage {

  event_name: any;
  ev_id: any;
  btnShow: any;
  eventSwitch: any = "event";

  images: Array<string> = [];
  grid: Array<Array<string>>;

  footerState: IonPullUpFooterState;

  review: any;
  file: any;
  toggleMediaView: any = false;
  allowPic: any = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public userService: UserService) {
    userService.eventMedia = false;
    this.event_name = navParams.get("event_name");
    this.ev_id = navParams.get("ev_id");
    this.btnShow = navParams.get("btnShow");
    this.footerState = IonPullUpFooterState.Collapsed;
    this.fetch();
  }

  fetch(){
    this.userService.getEventDetails( this.ev_id );
    let i = 0;
    var time = setInterval(() => {
      if(this.userService.eventMedia != false){
        clearInterval(time);
        this.userService.eventMedia.forEach(element => {
          this.images[i] = element.url;
          i += 1;
        });
        this.setGrid();
      }
    }, 1000);
  }

  addPic(){
    this.userService.setEventMedia( this.ev_id, this.event_name, "event" );
  }

  getPic(){
    this.userService.getPic( this.event_name );
  }

  submitReview(){
    let prompt = this.alertCtrl.create({
          title: 'Sumbit Review',
          message: "What's Your Review About This Event",
          inputs: [
            {
              name: 'review',
              placeholder: 'Enter Your Review'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              handler: data => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Submit',
              handler: data => {
                this.userService.addReview( data.review, this.ev_id );
                console.log('Saved clicked',data);
                setTimeout(() =>{
                  this.userService.getEventReviews( this.ev_id );
                }, 2500);
            }
            }
          ]
        });
        prompt.present();    
  }

  userDetails( username ) {
    console.log( username );
    this.navCtrl.push( UserProfilePage, {
      username: username,
      type: "indi"
    } );
  }

  groupDetails( username ) {
    console.log( username );
    this.navCtrl.push( UserProfilePage, {
      username: username,
      type: "group"
    } );
  }

  mediaViewSwitch( file ){
    if( this.toggleMediaView == false ){
      this.toggleMediaView = true;
    } else {
      this.toggleMediaView = false;
    }
    this.file = file;
  }

  setGrid(){
    this.grid = Array(Math.ceil(this.images.length/2));
    let rowNum = 0; //counter to iterate over the rows in the grid

    for (let i = 0; i < this.images.length; i+=2) { //iterate images

      this.grid[rowNum] = Array(2); //declare two elements per row

      if (this.images[i]) { //check file URI exists
        this.grid[rowNum][0] = this.images[i] //insert image
      }

      if (this.images[i+1]) { //repeat for the second image
        this.grid[rowNum][1] = this.images[i+1]
      }

      rowNum++; //go on to the next row
    }
  }

  setLeader( ev_id, lead_id ){
    this.userService.setEventLeader( ev_id, lead_id[0] );
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  showImage( file, desc ){
    //PhotoViewer.show(file, desc);
    file = this.images.indexOf(file);
    this.navCtrl.push( SeeeImage, {
      allImg: this.images,
      imgId: file,
      desc:  desc

    });
    // PhotoViewer.show('https://mysite.com/path/to/image.jpg', 'My image title', {share: false});
  }

  changeCover() {
    console.log(this.userService.eventDetails.event_name);
  }

  ionViewDidLoad() {
    console.log('Hello EventDetailsPage Page');
  }

}

@Component({
  template: `
              <ion-slides style="background-color: #222" (ionSlideDidChange)="onSlideDidChange($event)" >
                <ion-slide *ngFor="let i of imgData ">
                  <img *ngIf="!load" [src]='i'>
                  <ion-label style="color: whitesmoke" *ngIf="!load">{{ desc }}</ion-label>
                  <div style="color: whitesmoke" *ngIf="load">Loading...</div>
                </ion-slide>
              </ion-slides>
              <ion-footer>
                <ion-buttons *ngIf="!load">
                    <button ion-button clear color="danger" icon-left (click)="close()" >
                      <ion-icon name="close-circle"></ion-icon>
                      Close
                    </button>
                  </ion-buttons>
              </ion-footer>
              `
})

export class SeeeImage {
  @ViewChild(Slides) slides: Slides;
  imgData: any;
  imgId: any;
  desc: any;
  load: any = true;

  constructor( public navCtrl: NavController, public userService: UserService, public navParams: NavParams ){
    this.imgData = navParams.get('allImg');
    this.imgId = navParams.get('imgId');
    this.desc = navParams.get('desc');
  }

  close() {
    this.navCtrl.pop(); 
  }

  onSlideDidChange(e) {
    let currentIndex = this.slides.getActiveIndex();
    console.log("Current index is", currentIndex);
    if( this.userService.eventMedia[currentIndex] != undefined ){
      this.desc = this.userService.eventMedia[currentIndex].event_desc;
    }
  }

  ionViewDidLoad() {
    setTimeout(() =>{
      this.slides.slideTo(this.imgId, 0);
      this.load = false
    }, 500);
  }
}
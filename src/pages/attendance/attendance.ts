import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, PopoverController, ActionSheetController } from 'ionic-angular';

import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-attendance',
  templateUrl: 'attendance.html'
})

export class AttendancePage {

  event_leader: any = [];
  selectedEvent: any = "Tap to Select Event";

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public actionSheetCtrl: ActionSheetController, public userService: UserService) {
    this.fetch();
  }

  fetch(){
    let same;
    this.userService.getEventLeader( null, this.userService.player.user_id, "attendance" );
    var time = setInterval(() =>{
        if( this.userService.event_leader != false ){
          clearInterval(time);
          this.userService.event_leader.forEach( element => {
            let el = element
            this.event_leader.forEach( element => {
              if( el.ev_id == element.ev_id ){
                  console.log("SAME");
                  same = true;
              }
            });
            if( same != true ){
              this.event_leader.push({ event_name: el.event_name, ev_id: el.ev_id });
              console.log(this.event_leader);
            }
            same = false;
          });
        }
    }, 500);
  }

  takeAttendance( ev_id ){
    this.userService.scanData = undefined;
    this.userService.takeAttendance( ev_id, 'barcode' );

    let time = setInterval(()=>{

        if(this.userService.scanData){
          clearInterval(time);
          if(this.selectedEvent.ev_id){
            this.setAttendance();
          } else {
            this.userService.presentToast('attend_id');
          }
        }

    }, 1000);

    // let actionSheet = this.actionSheetCtrl.create({
          // title: 'Take Attendance',
          // buttons: [
          //   {
          //     text: 'Barcode',
          //     handler: () => {
          //       this.userService.takeAttendance( ev_id, 'barcode' );
          //     }
          //   },{
          //     text: 'RFID',
          //     handler: () => {
          //       this.userService.takeAttendance( ev_id, 'rfid' );
          //     }
          //   },{
          //     text: 'Cancle',
          //     handler: () => {
          //       console.log("Cancle Cliked");
          //     }
          //   }
          // ]
        // });
        // actionSheet.present()
  }

  setAttendance(){
    console.log("Did IT");
    // this.userService.scanData = '0000000000';
    // let time = setInterval(()=>{

    //   if( this.userService.scanData != '0000000000' ){
    //     clearInterval(time);
        this.userService.setAttendance( this.selectedEvent.ev_id );   
      // }

    // },1000);
  }

  fpAuth(){
    // this.userService.fpAuth( 'decrypt' );
    // this.userService.scanData = '0000000000';
    // let time = setInterval(()=>{

    //   if( this.userService.scanData != '0000000000' ){
    //     clearInterval(time);
        this.userService.setAttendance( this.selectedEvent.ev_id );   
      // }

    // },1000);
  }

  event(){
    let popover = this.popoverCtrl.create(EventListMenu, { event_leader: this.event_leader });
    popover.onDidDismiss((data) =>{
      console.log(data);
      if(data){
        this.selectedEvent = data[0];
        // this.takeAttendance( data[0].ev_id )
      } else {
        console.log("Cancled");
      }
    });
    popover.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AttendancePage');
  }

}

@Component({
  template: ` 
  <ion-list radio-group>
  
      <ion-list-header>
        Select Event
      </ion-list-header>

      <ion-item *ngFor="let e of event_leader">
        <ion-badge item-left>{{ e.ev_id }}</ion-badge>
        <ion-label text-wrap>{{ e.event_name }}</ion-label>
        <ion-radio (click)="dismiss(e.event_name, e.ev_id)" [value]="e.ev_id"></ion-radio>
      </ion-item>

  </ion-list>
  ` 
})

export class EventListMenu {

  event_leader: any;

  constructor(public navParams: NavParams, public viewCtrl: ViewController){
    this.event_leader = navParams.get('event_leader');
  }

  dismiss( event_name, ev_id ){
    let data = []
    data.push({ event_name: event_name, ev_id: ev_id })
    this.viewCtrl.dismiss( data );
  }

}


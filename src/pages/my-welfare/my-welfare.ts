import { Component } from '@angular/core';
import {  NavController } from 'ionic-angular';

import { MyEventsPage } from '../my-events/my-events';
import { MyGroupsPage } from '../my-groups/my-groups';

@Component({
  selector: 'page-my-welfare',
  templateUrl: 'my-welfare.html'
})
export class MyWelfarePage {

  nav: any;

  myEvents = MyEventsPage;
  myGroups = MyGroupsPage;

  constructor(public navCtrl: NavController) {
    this.nav = navCtrl;
  }


  openMyZone( zone ){
    if( zone == "events"){
      this.nav.push( this.myEvents );
    } else if( zone == "groups") {
      this.nav.push( this.myGroups );
    } else {
      console.log(" Event or Groups Error !! ");
    }
    
  }

  ionViewDidLoad() {
    console.log('Hello MyWelfarePage Page');
  }

}

import { Component, ViewChild  } from '@angular/core';
import { NavController, NavParams, AlertController, Slides } from 'ionic-angular';
import { Injectable } from '@angular/core';
 
import { UserService } from '../../providers/user-service';

@Injectable()

@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html'
})
export class GalleryPage {

  images: Array<string> = [];
  grid: Array<Array<string>>;

  toggleMediaView: any = false;

  constructor(public navCtrl: NavController, navParams: NavParams, public userService: UserService, public alertCtrl: AlertController,) {
    userService.eventMedia = false;
    this.fetch();
  }


  fetch(){
    this.userService.getEventMedia( null, "gallery" );
    let i = 0;
    var time = setInterval(() => {
      if(this.userService.eventMedia != false){
        clearInterval(time);
        this.userService.eventMedia.forEach(element => {
          this.images[i] = element.url;
          i += 1;
        });
        this.setGrid();
      }
    }, 1000);
  }

  setGrid(){
    this.grid = Array(Math.ceil(this.images.length/2));
    let rowNum = 0; //counter to iterate over the rows in the grid

    for (let i = 0; i < this.images.length; i+=2) { //iterate images

      this.grid[rowNum] = Array(2); //declare two elements per row

      if (this.images[i]) { //check file URI exists
        this.grid[rowNum][0] = this.images[i] //insert image
      }

      if (this.images[i+1]) { //repeat for the second image
        this.grid[rowNum][1] = this.images[i+1]
      }

      rowNum++; //go on to the next row
    }
  }

  mediaViewSwitch( ){
    if(this.toggleMediaView == false){
      this.toggleMediaView = true;
    } else {
      this.toggleMediaView = false
    }
  }

  showImage( file, desc ){
    //PhotoViewer.show(file, desc);
    file = this.images.indexOf(file);
    this.navCtrl.push( SeeImage, {
      allImg: this.images,
      imgId: file,
      desc:  desc

    });
    // PhotoViewer.show('https://mysite.com/path/to/image.jpg', 'My image title', {share: false});
  }

  ionViewDidLoad() {
    console.log('Hello GalleryPage Page');
  }

}

@Component({
  template: `
              <ion-slides style="background-color: #222" (ionSlideDidChange)="onSlideDidChange($event)" >
                <ion-slide *ngFor="let i of imgData ">
                  <img *ngIf="!load" [src]='i'>
                  <ion-label style="color: whitesmoke" *ngIf="!load">{{ desc }}</ion-label>
                  <div style="color: whitesmoke" *ngIf="load">Loading...</div>
                </ion-slide>
              </ion-slides>
              <ion-footer>
                <ion-buttons *ngIf="!load">
                    <button ion-button clear color="danger" icon-left (click)="close()" >
                      <ion-icon name="close-circle"></ion-icon>
                      Close
                    </button>
                  </ion-buttons>
              </ion-footer>
              `
})

export class SeeImage {
  @ViewChild(Slides) slides: Slides;
  imgData: any;
  imgId: any;
  desc: any;
  load: any = true;

  constructor( public navCtrl: NavController, public userService: UserService, public navParams: NavParams ){
    this.imgData = navParams.get('allImg');
    this.imgId = navParams.get('imgId');
    this.desc = navParams.get('desc');
  }

  close() {
    this.navCtrl.pop(); 
  }

  onSlideDidChange(e) {
    let currentIndex = this.slides.getActiveIndex();
    console.log("Current index is", currentIndex);
    if( this.userService.eventMedia[currentIndex] != undefined ){
      this.desc = this.userService.eventMedia[currentIndex].event_desc;
    }
  }

  ionViewDidLoad() {
    setTimeout(() =>{
      this.slides.slideTo(this.imgId, 0);
      this.load = false
    }, 500);
  }
}

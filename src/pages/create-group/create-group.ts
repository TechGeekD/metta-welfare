import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-create-group',
  templateUrl: 'create-group.html'
})
export class CreateGroupPage {

  addMember: any = false;
  addMemberToggle: any;
  groupName: any = undefined;
  callFrom: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public viewCtrl: ViewController) {
    this.addMemberToggle = navParams.get('toggle');
    this.callFrom = navParams.get('type');
    this.fetch();
  }

  fetch(){
    this.userService.getAllUsersList();
  }

  dismiss(){
    if( this.callFrom == "myGroup" ){
      this.viewCtrl.dismiss(this.groupName);
    } else if( this.callFrom == "groupInfo" ){
      this.viewCtrl.dismiss(null);
    }
    this.userService.foundUser = undefined;
  }

  addGroupMember(){
    console.log("Adding Member");
    console.log(this.addMemberToggle);
    if( this.addMemberToggle == false ){
      this.addMemberToggle = true;
      this.userService.getAllUsersList();
    } else {
      this.addMemberToggle = false;
    }
  }

  findUser( username ){
    this.userService.findUser( username );
  }

  createGroup(){
    if( this.addMember == true ){
      console.log("Creating Group");
      if( this.groupName != undefined ){
        this.userService.createGroup( this.groupName, 'createGroup' );
        this.dismiss();
        this.addMember = false;      
      } else {
        console.log("Create Group Error: Provide Group Name");
        this.userService.presentToast('addGroupName');  
      }
    } else {
      console.log("Create Group Error: Add Member");
      this.userService.presentToast('addMemberError');
    }
  }

  addUser( username ){
    if( this.callFrom == "myGroup" ){
      this.userService.addUser2Group( username );
      this.addMember = true;
      this.addGroupMember();
      this.userService.foundUser = undefined;
    } else if( this.callFrom == "groupInfo" ) {
      this.viewCtrl.dismiss( username );
      this.userService.foundUser = undefined;
    }
  }

  removeUser( username ){
    console.log("Removing User:"+username);
    //this.userService.groupMember.pop({ uname: username });
    var index = this.userService.groupMember.map(function(d) { return d['uname']; }).indexOf(username);
    this.userService.groupMember.splice(index, 1);
    console.log(index);
  }

  ionViewDidLoad() {
    console.log('Hello CreateGroupPage Page');
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { EventListPage } from '../event-list/event-list'

@Component({
  selector: 'page-event-category',
  templateUrl: 'event-category.html'
})
export class EventCategoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) { }


  openEventList( event ){
    this.navCtrl.push( EventListPage, {
      eventType : event,
      group : this.navParams.get("group"),
      group_id : this.navParams.get("group_id"),
      type : this.navParams.get("type")
    } );
  }


  ionViewDidLoad() {
    console.log('Hello EventCategoryPage Page');
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ActionSheetController, ModalController, AlertController } from 'ionic-angular';
import { Deploy } from '@ionic/cloud-angular';

import { UserService } from '../../providers/user-service';

import { HomePage } from '../home/home';
import { EventListPage } from '../event-list/event-list'

@Component({
  selector: 'page-user-info',
  templateUrl: 'user-info.html'
})
export class UserInfoPage {

  update: any = undefined;

  constructor(public navCtrl: NavController, public deploy: Deploy, public userService: UserService, public modalCtrl:  ModalController, public alertCtrl: AlertController, public actionSheetCtrl: ActionSheetController, public viewCtrl: ViewController) {
    console.log(userService.player)
    console.log(this.deploy.channel);
   }

  dismiss(){
      this.viewCtrl.dismiss();    
  }

  changeDP(){

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon: 'images',
          handler: () => {
              this.userService.managePic( "NULL", "lib" ); 
          }
        },
        {
          text: 'Use Camera',
          icon: 'aperture',
          handler: () => {
              this.userService.managePic( "NULL", 'cam' );
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  deleteSnapshots(){
    console.log("getSnapshots");
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Perform CleanUp',
      buttons: [
        {
          text: 'Get Snapshots List',
          handler: () => {
               this.deploy.getSnapshots().then((snapshots) => {
                  // snapshots will be an array of snapshot uuids
                  console.log(snapshots)
              });
          }
        },
        {
          text: 'Delete Snapshots',
          role: 'destructive',
          handler: () => {
               this.deploy.getSnapshots().then((snapshots) => {
                  // snapshots will be an array of snapshot uuids
                  console.log(snapshots)
                  this.deploy.info().then((data) =>{
                      console.log(data);
                      snapshots.forEach(element => {
                        if(data.deploy_uuid != element) {
                          this.deploy.deleteSnapshot(element).then(()=>{
                            console.log("Deleted:", element);
                          });  
                        } else {
                          console.log("Not Deleting:", element);
                        }
                      });
                  })
              });
          }
        }, 
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  checkUpdate(){
    console.log("cool")
    this.update = 'updating';

    this.deploy.check().then((snapshotAvailable: boolean) => {
      console.log("cool2");
      this.update = snapshotAvailable;
      if (snapshotAvailable) {
        console.log("cool3");
      }
    });
  
  }

  downloadUpdate(){
    this.update = "downloading";
    this.deploy.download().then(() => {
      this.update = "patch";
      return this.deploy.extract().then(() =>{
        this.update = "load_update";
        this.applyUpdate();
      });
    }, function(progress) {
        // Do something with the download progress
        console.log('Download Progress: ' + progress);
    });
  }

  applyUpdate(){
      let confirm = this.alertCtrl.create({
      title: 'Restart The App?',
      message: 'Do you agree to restart this app to complete the update process?',
      buttons: [
        {
          text: 'Later',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Update',
          handler: () => {
            this.deploy.load();
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  changeChannel(e){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Change Update Channel',
      buttons: [
        {
          text: 'production',
          handler: () => {
              this.deploy.channel = "production";
              this.update = undefined;
          }
        },
        {
          text: 'dev',
          handler: () => {
              this.deploy.channel = "dev";
              this.update = undefined;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  resetUpdate(){
    this.update = undefined;
  }

  deleteAccount(){
    var testCode = "deactive";
      this.userService.presentConfirm( testCode);
      var time = setInterval(() => {
        if (this.userService.logStatus == false) {
          clearInterval(time);
          this.navCtrl.setRoot(HomePage).catch(error => {
            console.log(error);
          });
        } else if ( this.userService.logStatus == "error" ) {
          clearInterval(time);
        }
      }, 500);
  }

  moreSettings( type ){
    if( type != 'f' ) {

      let profileModal = this.modalCtrl.create(MoreSettings, { type: type });
      // profileModal.onDidDismiss(data => {
      //   console.log(data);

      // });
      profileModal.present();
    } else {

      let actionSheet = this.actionSheetCtrl.create({
      title: 'Manage Fingerprint',
      buttons: [
        {
          text: 'Add Fingerprint',
          icon: 'add',
          handler: () => {
              this.userService.fpAuth( 'encrypt' );
          }
        },
        {
          text: 'Remove Fingerprint',
          role: 'destructive',
          icon: 'remove-circle',
          handler: () => {
              this.userService.fpAuth( 'delete' );
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
    }
  }

  ionViewDidLoad() {
    console.log('Hello UserInfoPage Page');
  }

}


@Component({
  template: `
  <ion-header>

    <ion-navbar color="secondary">
        <ion-title>More Settings</ion-title>
        <ion-buttons end>
            <button *ngIf="!toggleInterest" ion-button clear (click)="dismiss()">Close
                <ion-icon name="close-circle" padding-left></ion-icon>
            </button>
            <button *ngIf="toggleInterest" ion-button clear (click)="toggleInterestMenu( null )">Close
                <ion-icon name="close-circle" padding-left></ion-icon>
            </button>
        </ion-buttons>
    </ion-navbar>

  </ion-header>

  <ion-content style="background-color: #222">
      <div *ngIf="!toggleInterest && type == 'i'" >
        <ion-list>
            <ion-list-header style="margin-bottom: 0%" color="light"><p>Your Interests</p></ion-list-header>
            <div *ngIf="userService.interest && userService.interest != 'err'">
              <ion-item (click)="gotoEventList( i.interest )" *ngFor="let i of userService.interest">
                {{ i.interest}}
              </ion-item>
            </div>
        </ion-list>
        <ion-fab bottom right>
          <button ion-fab color="light" (click)="toggleInterestMenu( null )"><ion-icon name="add"></ion-icon></button>
        </ion-fab>
      </div>
      <div *ngIf="toggleInterest && type == 'i'">
        <ion-list>
            <ion-list-header style="margin-bottom: 0%" color="light"><p *ngIf="type== 'i'">Add Interest</p></ion-list-header>
            <ion-grid margin-left margin-right>
              <ion-row *ngFor="let row of grid">
              <ion-col witdh-70 *ngFor="let interest of row">
                <ion-badge margin-top (click)="toggleInterestMenu( interest )" *ngIf="interest">
                  <ion-icon item-left name="add"></ion-icon>
                  {{ interest }}
                </ion-badge>
              </ion-col>
              </ion-row>
            </ion-grid>
        </ion-list>
      </div>
      <div *ngIf="type == 'd'">
        <ion-list>
          <ion-list-header style="margin-bottom: 0%" color="light">
          <p>Your Details</p>
          </ion-list-header>
          <ion-item>
            <ion-label color="primary">Username: </ion-label>
            <ion-input [readonly]="readonlys( 'i' )" [(ngModel)]="userService.player.uname" [value]="userService.player.uname">
            </ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="primary">Password: </ion-label>
            <ion-input type='password' [readonly]="readonlys( 'i' )" [(ngModel)]="userService.player.passwordx" [value]="userService.player.passwordx">
            </ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="primary">Full Name: </ion-label>
            <ion-input [readonly]="readonlys( 'i' )" [(ngModel)]="userService.player.fullname" [value]="userService.player.fullname">
            </ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="primary">E-Mail Id: </ion-label>
            <ion-input [readonly]="readonlys( 'i' )" [(ngModel)]="userService.player.email" [value]="userService.player.email">
            </ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="primary">Phone No: </ion-label>
            <ion-input [readonly]="readonlys( 'i' )" [(ngModel)]="userService.player.phone" [value]="userService.player.phone">
            </ion-input>
          </ion-item>
        </ion-list>
        <ion-fab bottom right>
          <button ion-fab color="light" (click)="readonlys( null )" item-right icon-only>
            <ion-icon *ngIf="readonly" name="create"></ion-icon>
            <ion-icon *ngIf="!readonly" name="checkmark-circle"></ion-icon> 
          </button>
        </ion-fab>
      </div>
  </ion-content>
  `
})

export class MoreSettings{
   
    type: any;
    toggleInterest: any = false;
    interest: Array<string> = [];
    grid: Array<Array<string>>;
    readonly: any = true;

    constructor(public navCtrl: NavController, public navParams: NavParams,public userService: UserService, public viewCtrl: ViewController) {
        this.type = navParams.get('type');
        if( this.type == 'i' ){
          this.userService.getUserInterest();
          this.userService.getAllInterest();
        }
        var time = setInterval(()=>{ 
            let same = false
            if( userService.interestAll ){
                clearInterval(time);
                userService.interestAll.forEach(element => {
                  let el = element;
                  this.interest.forEach(element => {
                    if( el.interest == element ) {
                      console.log("SAME");
                      same = true;
                    }
                  });
                  if( same == false ){
                    this.interest.push(el.interest);
                    console.log(this.interest);
                  }
                  same = false;
                });
                console.log(this.interest);
                this.setGrid();
            }
        },1000);
    }

    toggleInterestMenu( t ){
      if( this.toggleInterest == false ){
        this.toggleInterest = true;
      } else {
        this.toggleInterest = false;
        this.userService.getUserInterest();
      }

      if( t != null ){
        console.log(t);
        this.userService.setInterest(t);
      }

    }

    setGrid(){
      this.grid = Array(Math.ceil(this.interest.length/3));
      let rowNum = 0; //counter to iterate over the rows in the grid

      for (let i = 0; i < this.interest.length; i+=3) { //iterate data

        this.grid[rowNum] = Array(3); //declare three elements per row

        if (this.interest[i]) { //check file URI exists
          this.grid[rowNum][0] = this.interest[i] //insert data
        }

        if (this.interest[i+1]) { //repeat for the second data
          this.grid[rowNum][1] = this.interest[i+1]
        }

        if(this.interest[i+2]) { //repeat for the three data
          this.grid[rowNum][2] = this.interest[i+2];
        }

        rowNum++; //go on to the next row
      }
    }

    gotoEventList( interest ){
      this.navCtrl.push(EventListPage, {
        type: 'interest',
        interest: interest
      });
    }

    readonlys( r ){
      if( r == null){
        if( this.readonly == true ){
          this.readonly = false;
        } else {
          this.userService.presentConfirm("updateEvent");
          let time = setInterval(() =>{
            if( this.userService.updateStatus == true ){
                clearInterval(time);
                console.log(this.userService.player);
                this.userService.setUserProfile();
                this.readonly = true;
            } else {
              console.log('Update Status False');
            }
          },2000);
        }
      }
      return this.readonly;

      }

    dismiss(){
        this.viewCtrl.dismiss();    
    }
}
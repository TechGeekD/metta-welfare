import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, AlertController } from 'ionic-angular';
// import { Headers, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { UserService } from '../../providers/user-service';

import { EventDetailsPage } from '../event-details/event-details';

@Injectable()

@Component({
  selector: 'page-my-events',
  templateUrl: 'my-events.html'
})
export class MyEventsPage {
@ViewChild(Content) content: Content;

  http;
  nav;

  arrayOfKeys: any;
  events: any;
  
  json: any;
  uname: any;
  event_name: any;

  eventDetail = EventDetailsPage;

  deleteStatus: any = false;
  eventSwitch: any = "current";
  time: any;

  see: any = [{
    id: undefined,
    event_name: undefined,
    uname: undefined
  }];
  seeEventStatus: boolean = true;

  constructor(public navCtrl: NavController, navParams: NavParams, public userService: UserService, public alertCtrl: AlertController /*, httpService: Http*/) {
    // this.http = httpService;
    this.nav = navCtrl;
    this.uname = this.userService.username;
    //this.events = navParams.get("events");
    this.fetch();
  }

  // private headers = new Headers({
  //   'Content-Type': 'application/json',
  //   'Cache-Control': 'no-cache',
  //   'Access-Control-Allow-Origin': '*',
  //   'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
  //   'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
  //   'Access-Control-Max-Age': '86400'
  // });

  fetch() {
    // this.json = JSON.stringify({ uname: this.uname, event_name: this.event_name });
    // console.log(this.json);
    // this.userService.url = this.userService.server + '/events/joined';
    // console.log("REST API URL: " + this.userService.url);
    
    // this.http.post(this.userService.url, this.json, { headers: this.headers })
    //   .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
    //     if (data) {
    //       this.events = data;
    //       console.log("events1:");
    //       console.log(this.events);
    //     } else {
    //       console.log("NO DATA FETCHED !!");
    //     }
        
    //   });
    this.userService.getEventReqStatus( null );
  }

  seeEvents(events: any) {
    console.log("events2:");
    console.log(events);
    console.log("seeEventStatus1:");
    console.log(this.seeEventStatus);
    if (events != false) {
      if (this.see.event_name == events.event_name && this.seeEventStatus == true) {
        this.seeEventStatus = false;
      } else {
        console.log("events3:");
        console.log(events);
        for (var x in events) {
          this.see = events;
        }
        console.log("seeEventStatus2:");
        this.seeEventStatus = true;
        console.log("events4:");
        console.log(events);
        console.log("X2:");
        console.log(x);
      }
    } else {
      this.seeEventStatus = false;
      console.log("seeEventStatus2:");
      console.log(this.seeEventStatus);
    }
  }

  eventDetails( ev_id: any ){
    console.log( ev_id )
    this.nav.push( this.eventDetail, {
      ev_id: ev_id,
      //btnShow: this.btnShow
    } ).catch(error => {
      console.log(error);
    });
  }

  scroll(events: any) {
    this.seeEvents(events);
    setTimeout(() => {
      let element = document.getElementById(events.event_name);
      this.content.scrollTo(0, element.offsetTop - 100, 800);
    }, 0);
  }

  deleteEvent( event_name ) {
    this.userService.deleteUsersEvent( event_name );
    console.log("DELETE STATUS iS: "+this.deleteStatus);
    this.time = setInterval(() => {
      this.fetch();
      this.deleteStatus = this.userService.deleteStatus;
      console.log("DELETE STATUS iS: "+this.deleteStatus);
      if (this.deleteStatus == true) {
        clearInterval(this.time);
        this.deleteStatus = false;
      }
   }, 3000);
  }

  // private handleError(error: Response) {
  //   console.error(error);
  //   return Observable.throw(error.json().error || 'Server error');
  // }

  ionViewDidLoad() {
    console.log('Hello MyEventsPage Page');
  }

}

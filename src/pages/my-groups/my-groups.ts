import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

import { UserService } from '../../providers/user-service';

import { CreateGroupPage } from '../create-group/create-group';
import { GroupInfoPage } from '../group-info/group-info';

@Component({
  selector: 'page-my-groups',
  templateUrl: 'my-groups.html'
})
export class MyGroupsPage {

  groupInfo = GroupInfoPage;

  group: any;
  time: any;
  err: any = [];

  constructor(public navCtrl: NavController, public userService: UserService, public modalCtrl: ModalController) {
    this.fetch();
  }

  fetch(){
    this.userService.getGroupList();
    var time = setInterval(() =>{
      if(this.userService.groupsList){
        clearInterval(time);
        this.checkAvailableGroup();    
      }  
    }, 1000);
    
  }

  checkAvailableGroup(){
    this.userService.groupsList.forEach(element => {
      if( element.mem_type == 'leader' ){
        this.err[0] = 1;
      }
      if( element.mem_type == 'member' ){
        this.err[1] = 1;
      }
    });
    if( this.err[0] != 1 ){
      this.err[0] = 0;
    }
    if( this.err[1] != 1 ){
      this.err[1] = 0;
    }
  }

  groupDetails( group ){
    this.userService.groupInfo = undefined;
    this.navCtrl.push( this.groupInfo, {
      group: group
    } );
  }

  presentProfileModal() {
   let profileModal = this.modalCtrl.create(CreateGroupPage, { toggle: false, type: "myGroup" });
   profileModal.onDidDismiss(data => {
     console.log(data);
     this.group = data;
     this.fetch();
   });
   profileModal.present();
  }

  removeGroup( group_name ){
      this.userService.removeGroup( group_name );
      
      this.time = setInterval(() => {
        if( this.userService.deleteStatus == true ) {
          clearInterval(this.time);
        }
          this.fetch();
      }, 500);
  }
 
  ionViewDidLoad() {
    console.log('Hello MyGroupsPage Page');
  }

}

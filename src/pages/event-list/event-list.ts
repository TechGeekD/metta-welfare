import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, AlertController, ActionSheetController, ViewController, PopoverController } from 'ionic-angular';
import { Headers, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { UserService } from '../../providers/user-service';
import { EventDetailsPage } from '../event-details/event-details';

@Injectable()

@Component({
  selector: 'page-event-list',
  templateUrl: 'event-list.html'
})
export class EventListPage {
  @ViewChild(Content) content: Content;

  http;
  nav;

  arrayOfKeys: any;
  events: any;
  eventsAll: any;
  
  json: any;
  ev_id: any;
  event_namex: any;
  event_name: any;
  event_desc: any;
  event_city: any;

  eventDetail = EventDetailsPage;

  deleteStatus: any = false;
  time: any;
  btnShow: any = false;
  category: any;
  group: any;
  group_id: any = null;
  callFrom: any;
  filterTerm: string = '';
  interest

  eventCity: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public actionSheetCtrl: ActionSheetController, public popoverCtrl: PopoverController, public alertCtrl: AlertController, httpService: Http) {
    this.http = httpService;
    this.nav = navCtrl;
      this.group = navParams.get("group");
      this.group_id = navParams.get("group_id");
      this.callFrom = navParams.get("type");
      this.category = navParams.get("eventType");
      this.interest = navParams.get('interest');
      
      this.fetch();
  }

  private headers = new Headers({
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
    'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
    'Access-Control-Max-Age': '86400'
  });

  fetch() {
    if( this.callFrom != 'interest' ){
      this.userService.url = this.userService.server + '/events';
    } else {
      this.userService.url = this.userService.server + '/events/interest/ev_list';
    }
    console.log("REST API URL: " + this.userService.url);

    this.json = JSON.stringify({ category: this.category, interest: this.interest });

    this.http.post(this.userService.url, this.json, { headers: this.headers })
      .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
        if (data) {
          this.eventsAll = data;
          this.events = data
          this.getFilterCity();
          this.convert();
          console.log("events1:");
          console.log(this.events);
        } else {
          console.log("NO DATA FETCHED !!");
        }
      });
      
  }

  convert(){
      let monthNames = [
              "Jan", "Feb", "Mar",
              "Apr", "May", "Jun", "Jul",
              "Aug", "Sep", "Oct",
              "Nov", "Dec"
            ];
      let i = 0;
      this.events.forEach(element => {
        let date = new Date(element.event_date);
        let day = date.getDate();
        let monthIndex = date.getMonth();
        let year = date.getFullYear();
        this.events[i].event_date = day + ' ' + monthNames[monthIndex] + ' ' + year;
        i += 1;
      });
  }

  filterItems(searchTerm){
 
        return this.eventsAll.filter((item) => {
            return item.event_city.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });     
 
  }

  setFilteredItems( filterTerm ) {
      console.log(filterTerm);
      this.events = this.filterItems( filterTerm );

  }

  getFilterCity(){
    var i = 0;
    this.eventsAll.forEach(element => {
      this.eventCity[i] = element.event_city;
      i++;
    });
    console.log(this.eventCity);
  }

  presentPopover(ev) {

    let popover = this.popoverCtrl.create(PopoverPage, {
      cityList : this.eventCity
    });


    popover.onDidDismiss( data => {
      if( data != null && data != 'all' )
        this.setFilteredItems( data );
      else if ( data == 'all' )
        this.setFilteredItems( '' );
    });

    popover.present({
      ev: ev
    });
  }

  adminUser() {
    //this.seeEvents(events);
    if(this.btnShow == false){
      this.btnShow = true;
    } else {
      this.btnShow = false;
    }
    // setTimeout(() => {
    //   let element = document.getElementById(events.event_name);
    //   this.content.scrollTo(0, element.offsetTop - 100, 800);
    // }, 0);
  }

  eventDetails( event_name: any, ev_id: any ){
    console.log( event_name )
    this.nav.push( this.eventDetail, {
      event_name: event_name,
      ev_id: ev_id,
      btnShow: this.btnShow
    } ).catch(error => {
      console.log(error);
    });
  }

  setEventStatus( ev_id ){
      console.log(ev_id);
      // this.userService.getEventLeader( ev_id, null, "event" );
      // if( this.userService.event_leader == this.userService.player.user_id && this.userService.event_leader != "err" && this.userService.event_leader != null ){
        let actionSheet = this.actionSheetCtrl.create({
          title: 'Set Event Status',
          buttons: [
            {
              text: 'Active',
              handler: () => {
                this.userService.setEventStatus( ev_id, 'active' );
              }
            },{
              text: 'Completed',
              handler: () => {
                this.userService.setEventStatus( ev_id, 'completed' );
              }
            },{
              text: 'Deactive',
              role: 'destructive',
              handler: () => {
                this.userService.setEventStatus( ev_id, 'deactive' );
              }
            },{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        actionSheet.present();
      // } else {
        // console.log("NOT EVENT LEADER");
      // }
      // this.userService.event_leader = null;
  }

  addEvent(){

    let prompt = this.alertCtrl.create({
      title: 'Add New Event',
      message: "Add Event Details",
      inputs: [
        {
          name: 'event_name',
          placeholder: 'Name'
        },
        {
          name: 'event_desc',
          placeholder: 'Description'
        },
        {
          name: 'event_city',
          placeholder: 'City'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.event_name = data.event_name;
            this.event_desc = data.event_desc;
            this.event_city = data.event_city;
            this.addEvent2db("insert");
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  
  }

  addEvent2db( insert_update ){

    this.json = JSON.stringify({ user_id: this.userService.player.user_id, ev_id: this.ev_id, event_namex: this.event_namex, event_name: this.event_name, event_desc: this.event_desc, event_city: this.event_city, type: "indi", category: this.category });
    console.log(this.json);
    this.userService.url = this.userService.server + '/events/add';

    if( insert_update == "insert" ){
      this.http.post(this.userService.url, this.json, { headers: this.headers }).subscribe(data => {
        this.json = data;
        console.log("post: ");
        console.log(data);
        this.fetch();
        this.userService.presentToast("eventAdd");
      }, error => {
        this.userService.presentToast("eventAddError");
        console.log("Oooops!" + error);
      });
    } else if ( insert_update == "update" ){
      this.http.put(this.userService.url, this.json, { headers: this.headers }).subscribe(data => {
        this.json = data;
        console.log("post: ");
        console.log(data);
        //location.reload();
        this.fetch();
        this.userService.presentToast("eventUpdate");
      }, error => {
        this.userService.presentToast("eventUpdateError");
        console.log("Oooops!" + error);
      });
    } else if( insert_update == "join" ){
      if( this.callFrom != 'group' ){
        this.userService.url = this.userService.server + '/events/join';
        console.log("COOLL");
      } else {
        this.userService.url = this.userService.server + '/events/group/join';
        this.json = JSON.stringify({ group_id: this.group_id, ev_id: this.ev_id, type: "group" });
        console.log(this.json);
      }
      console.log("REST API URL: "+this.userService.url);

      this.http.post(this.userService.url, this.json, { headers: this.headers }).subscribe(data => {
        this.json = data;
        console.log("post: ");
        console.log(data);
        
        this.userService.presentToast("eventJoin");
        if( this.callFrom == "group" ){
          this.userService.groupsEvents( this.group.group_name );
        }
      }, error => {
        
        this.userService.presentToast("eventJoinError");
        console.log("Oooops!" + error);
      });
    } else {
      console.log("ERROR: Event insert_update ");
    }
    
  }

  editEvent( event_name ){
    this.event_namex = event_name;
    console.log( event_name );
    for( var x in this.events ){
      if( this.events[x].event_name == event_name ){
    let prompt = this.alertCtrl.create({
      title: 'Update Event',
      message: "Update Event Details",
      inputs: [
        {
          name: 'event_name',
          placeholder: 'Name',
          value: this.events[x].event_name
        },
        {
          name: 'event_desc',
          placeholder: 'Description',
          value: this.events[x].event_desc
        },
        {
          name: 'event_city',
          placeholder: 'City',
          value: this.events[x].event_city
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.event_name = data.event_name;
            this.event_desc = data.event_desc;
            this.event_city = data.event_city;
            this.addEvent2db("update");
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();        

      }
    }
  }

  joinEvent( event_name, event_id ) {
    this.event_name = event_name;
    this.ev_id = event_id;
    this.addEvent2db("join");
  }

  joinRequest( ev_id ) {
    this.userService.joinRequest( ev_id, this.group_id );
  }

  deleteEvent( event_name ) {
    this.userService.deleteEvent( event_name );
    console.log("DELETE STATUS iS: "+this.deleteStatus);
    this.time = setInterval(() => {
      this.fetch();
      this.deleteStatus = this.userService.deleteStatus;
      console.log("DELETE STATUS iS: "+this.deleteStatus);
      if (this.deleteStatus == true) {
        clearInterval(this.time);
        this.deleteStatus = false;
      }
   }, 3000);
  }

  // doRefresh() {
    //   console.log('Begin async operation');
    //   this.fetch();
    //   setTimeout(() => {  
    //     console.log('Async operation has ended');
    //   }, 2000);
  // }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  ionViewDidLoad() {
    console.log('Hello EventListPage Page');
  }

}

//popup menu
@Component({
  template: `
            <ion-list >
                <ion-item *ngFor='let city of cityList' (click)="filter(city)">
                  {{ city }}
                </ion-item>
                <ion-item (click)="filter('all')">All</ion-item>
            </ion-list>
  `
})

export class PopoverPage {


  cityListAll: any = [];
  cityList: any = [];

  constructor(private navParams: NavParams, public viewCtrl: ViewController) {

  }

  ngOnInit() {
    if (this.navParams.data) {
      this.cityListAll = this.navParams.data.cityList;
      this.cityListAll.forEach(element => {
        let x = element;
        let copy = true;
        this.cityList.forEach(element => {
          
          if( element == x ){
            copy = false
          }

        });

        if( copy == true ){
          this.cityList.push(x);
        } 

      });
    }   
  }
  
  filter( city ){
    this.viewCtrl.dismiss( city );
  }


}

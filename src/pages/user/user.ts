import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController } from 'ionic-angular';
import { Headers, Http } from '@angular/http';
import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { IonPullUpFooterState } from 'ionic-pullup';
import { FileChooser } from '@ionic-native/file-chooser';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { UserService } from '../../providers/user-service';

import { HomePage } from '../home/home';
import { UserInfoPage } from '../user-info/user-info';
import { NewsfeedPage } from '../newsfeed/newsfeed'


@Injectable()

@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  http;
  nav;

  homePage = HomePage;

  footerState: IonPullUpFooterState;

  textShow: boolean = true;
  player: userdatax;
  logStatus: any;
  activein: any;
  allEvents: any;
  testCode: any;
  time: any;
  uname: any;
  unamex: any;
  json: any;
  menu: any;

  constructor(public navCtrl: NavController, private fileChooser: FileChooser, public userService: UserService, public modalCtrl: ModalController, public storage: Storage, menuCtrl: MenuController, navParams: NavParams, private httpService: Http) {
    console.log(userService.player.uname);
    if( userService.player.uname == undefined ){
      this.goToHome();
    } else {
      this.player = userService.getPlayer();
      setTimeout(()=>{
        this.fetch();
      },500);
      
    }
    console.log(this.player);
    this.nav = navCtrl;
    this.http = httpService;
    this.menu = menuCtrl;
    this.menu.enable(true);
    this.footerState = IonPullUpFooterState.Collapsed;
  }

  fetch(){
    this.userService.url = this.userService.server + "/event/count";
    this.json = JSON.stringify({ user_id: this.player.user_id, group: undefined});
    this.http.post(this.userService.url, this.json, { headers: this.headers }).subscribe(data => {
      data = JSON.parse(data._body)
      console.log(data);
      this.activein = data.activeIn;
      
    }, error => {
      console.log("Oooops!" + error);
    });
    this.userService.url = this.userService.server + "/event/all/count";
    this.json = JSON.stringify({ user_id: this.player.user_id, group: undefined});
    this.http.post(this.userService.url, this.json, { headers: this.headers }).subscribe(data => {
      data = JSON.parse(data._body)
      console.log(data);
      this.allEvents = data.allEvents;
      
    }, error => {
      console.log("Oooops!" + error);
    });
  }

  profile() {
    console.log( this.userService.username );
    // this.userService.presentToast("test");
    let profileModal = this.modalCtrl.create(UserInfoPage, { toggle: false, type: "myGroup" });
    profileModal.onDidDismiss(data => {
      // console.log(data);

    });
    profileModal.present();
  }
  
  tapEvent(e){

    if( this.userService.server == 'http://192.168.1.3:90' ) {

      this.userService.server = 'http://192.168.43.79:90';

    } else if ( this.userService.server == 'http://192.168.43.79:90' )  {

      this.userService.server = 'https://evening-waters-53433.herokuapp.com';

    } else if ( this.userService.server == 'https://evening-waters-53433.herokuapp.com' ) {

      this.userService.server = 'http://localhost:90'; 

    } else if ( this.userService.server == 'http://localhost:90' ) {

      this.userService.server = 'http://192.168.1.3:90';

    }
    
    console.log(this.userService.server);
    this.userService.presentToast( "serverSwitch" )
  }

  // ionViewCanLeave(): boolean{
    //  // here we can either return true or false
    //  // depending on if we want to leave this view
    //  if(this.player.empno){
    //     return false;
    //   } else {
    //     return true;
    //   }
  // }

  edit() {

    this.unamex = this.player.uname;
    console.log(this.unamex);
    
    this.testCode = "updated";
    if (this.textShow) {
      this.textShow = false;
    } else {
      this.textShow = true;

      this.json = JSON.stringify({ unamex: this.unamex, uname: this.player.uname, fullname: this.player.fullname, passwordx: this.player.passwordx, email: this.player.email, phone: this.player.phone, statusx: this.player.statusx });
      this.userService.url = this.userService.server + "/users";

      this.http.put(this.userService.url, this.json, { headers: this.headers }).subscribe(data => {
        this.json = data._body;
        this.uname = this.player.uname;
        console.log(data);
      }, error => {
        console.log("Oooops!" + error);
      });
      this.userService.presentToast(this.testCode);
    }
    
  }

  private headers = new Headers({
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
    'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
    'Access-Control-Max-Age': '86400'
  });

  newsfeed() {
    // this.testCode = "deactive";
      // this.userService.presentConfirm(this.testCode);
      // this.time = setInterval(() => {
      //   if (this.userService.logStatus == false) {
      //     clearInterval(this.time);
      //     this.goToHome();
      //   } else if ( this.logStatus == "error" ) {
      //     clearInterval(this.time);
      //   }
      // }, 500);
    let profileModal = this.modalCtrl.create(NewsfeedPage, { toggle: false, type: "myGroup" });
    // profileModal.onDidDismiss(data => {
    //   console.log(data);

    // });
    profileModal.present();
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    
    this.userService.getNotify();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 700);
  }

  clearNotify(){
    this.storage.get("MaxNotifyId").then((val) =>{
      if( val != null ){
        var max = val;
      } else {
        var max: any = 0;
      }
      this.userService.notify.forEach(element => {
        if( element.nid > max ){
          max = element.nid;
        }
      });
      this.storage.set('MaxNotifyId',max);
      console.log("MAX: "+max);
      this.userService.notify = false;
      this.userService.setMaxNotifyId(max);
    });
  }
  
  ionViewDidLoad() {
    console.log('Hello UserPage Page');
  }

  goToHome() {
    this.player.uname = undefined;
    this.nav.setRoot(this.homePage).catch(error => {
      console.log(error);
    });
  }

}

export class userdatax {

  constructor(public user_id?: number,
    public uname?: string,
    public fullname?: string,
    public passwordx?: string,
    public email?: string,
    public phone?: number,
    public statusx?: string,
    public dp?: string,
    public error?: string) { }
}

// @Component({
//   template: ` <ion-list>
//                 <ion-item (click)="close('file')">Choose File</ion-item>
//                 <ion-item (click)="close('cam')">Camera</ion-item>                
//               </ion-list>
  
//   `
// })

// export class DPMenu {

//   constructor(public viewCtrl: ViewController){}

//   close(data) {
//     this.viewCtrl.dismiss(data);
//   }

// }
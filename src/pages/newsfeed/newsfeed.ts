import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-newsfeed',
  templateUrl: 'newsfeed.html'
})
export class NewsfeedPage {

  constructor(public navCtrl: NavController, public storage: Storage, public userService: UserService, public viewCtrl: ViewController) {
    this.userService.getNotify();
  }


  dismiss(){
      this.viewCtrl.dismiss();    
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    
    this.userService.getNotify();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 700);
  }

  clearNotify(){
    this.storage.get("MaxNotifyId").then((val) =>{
      if( val != null ){
        var max = val;
      } else {
        var max: any = 0;
      }
      this.userService.notify.forEach(element => {
        if( element.nid > max ){
          max = element.nid;
        }
      });
      this.storage.set('MaxNotifyId',max);
      console.log("MAX: "+max);
      this.userService.notify = false;
      this.userService.setMaxNotifyId(max);
    });
  }

  ionViewDidLoad() {
    console.log('Hello NewsfeedPage Page');
  }

}

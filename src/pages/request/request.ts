import { Component } from '@angular/core';
import { NavController,  PopoverController, ViewController } from 'ionic-angular';

import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-request',
  templateUrl: 'request.html'
})
export class RequestPage {

  requestTitle: any;
  requestDesc: any;
  reqCate: any = 'Select Category';

  constructor(public navCtrl: NavController, public userService: UserService, public popoverCtrl: PopoverController) {}

  submitRequest(){
    if( this.reqCate != 'Select Category' ){
      this.userService.submitRequest(this.requestTitle,this.requestDesc,this.reqCate);
    } else {
      console.log('Select Category First');
      this.userService.presentToast('reqSubmit_err');
    }
  }

  attach( a ){
      if( a == null ){
        this.userService.attachRequest();
      } else {
        this.userService.attachReq = undefined
      }
  }


  presentPopover(ev) {

    let popover = this.popoverCtrl.create(PopoverReqPage);


    popover.onDidDismiss( data => {
      if( data == null ){
        console.log("Error No Category")
      } else {
        this.reqCate = data;
        console.log(this.reqCate);
      }
    });

    popover.present({
      ev: ev
    });
  }

  ionViewDidLoad() {
    console.log('Hello RequestPage Page');
  }

}

@Component({
  template: `
            <ion-list >
                <ion-item (click)="filter('clean')">
                  Clean
                </ion-item>
                <ion-item (click)="filter('help')">
                  Help
                </ion-item>
                <ion-item (click)="filter('donate')">
                  Donate
                </ion-item>
                <ion-item (click)="filter('camp')">
                  Camp
                </ion-item>
            </ion-list>
  `
})

// popUp Menu
export class PopoverReqPage {

  constructor(public viewCtrl: ViewController) {

  }

  filter( category ){
    this.viewCtrl.dismiss( category );
  }


}

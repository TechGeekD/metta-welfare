import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html'
})
export class UserProfilePage {


  username: any;
  type: any;

  constructor(public navCtrl: NavController, public navParms: NavParams, public userService: UserService) {
    userService.userProfile = undefined;
    this.username = navParms.get("username");
    this.type = navParms.get("type");
    this.fetch();
  }

  fetch(){
    this.userService.getUserProfile( this.username, this.type );
  }

  userDetails( username ) {
    console.log( username );
    this.navCtrl.push( UserProfilePage, {
      username: username,
      type: "indi"
    } );
  }

  ionViewDidLoad() {
    console.log('Hello UserProfilePage Page');
  }

}

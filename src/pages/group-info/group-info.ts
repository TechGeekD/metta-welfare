import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';

import { UserService } from '../../providers/user-service';

import { CreateGroupPage } from '../create-group/create-group';
import { UserProfilePage } from '../user-profile/user-profile';
// import { EventListPage } from '../event-list/event-list';
import { EventCategoryPage } from '../event-category/event-category';
import { EventDetailsPage } from '../event-details/event-details';

@Component({
  selector: 'page-group-info',
  templateUrl: 'group-info.html'
})
export class GroupInfoPage {

  group: any;
  groupLeader: any = false;
  leader: any;
  group_id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public modalCtrl: ModalController) {
    this.group = navParams.get('group');
    this.fetch();
  }

  fetch(){
    this.userService.groupDetails( this.group.group_name );
    this.userService.groupsEvents( this.group.group_name );
    
    var time = setInterval(() =>{

      if( this.userService.groupInfo ){
        clearInterval( time );
        this.checkAdmin();
      }
    
    }, 700);

  }

  checkAdmin(){
    this.userService.groupInfo.forEach(element => {
      console.log(element.member);
      if( element.mem_type == 'leader' ){
        console.log("found leader");
        this.leader = element.member;
        this.group_id = element.group_id
        if( this.leader == this.userService.username ){
          this.groupLeader = true;
          console.log("its leader");
        }
      }
    } );
  }
  
  joinEvent( ){
    console.log( this.group );
    this.navCtrl.push( EventCategoryPage, {
      group: this.group,
      group_id: this.group_id,
      type: 'group'
    } );
  }

  userDetails( username ) {
    console.log( username );
    this.navCtrl.push( UserProfilePage, {
      username: username,
      type: "indi"
    } );
  }

  eventDetails( ev_id ){
    console.log( ev_id )
    this.navCtrl.push( EventDetailsPage, {
      ev_id: ev_id,
    } ).catch(error => {
      console.log(error);
    });
  }

  presentProfileModal() {
   let profileModal = this.modalCtrl.create(CreateGroupPage, { toggle: true, type: "groupInfo" } );
   profileModal.onDidDismiss(data => {
     console.log(data);
     if( data != null ){
      this.userService.createGroup( this.group.group_name, 'updateGroup', data );
     } else {
       console.log("Closed Add Member! ");
     }
     this.fetch();
   });
   profileModal.present();
  }

  removeGroupMember( member, group_name ){
    this.userService.removeGroupMember( member, group_name );
  }

  removeGroupEvent( event_name ){
    var x = this.userService.removeGroupEvent( event_name, this.group.group_name );
    console.log("REMOVE FT:",x)
    var time
    time = setInterval(() =>{
      if( this.userService.json == 'deleted' ){
          clearInterval(time);
      } else {
        this.userService.groupsEvents( this.group.group_name );
      }
    },700);
  }

  ionViewDidLoad() {
    console.log('Hello GroupInfoPage Page');
  }

}

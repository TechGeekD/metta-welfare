import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UserService } from '../../providers/user-service';

import { UserProfilePage } from '../user-profile/user-profile';

@Component({
  selector: 'page-find-user',
  templateUrl: 'find-user.html'
})
export class FindUserPage {


  userProfile = UserProfilePage;

  // foundUser: any;
  // filterTerm: any = [];

  constructor(public navCtrl: NavController, public userService: UserService) {
    this.fetch();
  }

  findUser(username: any) {
    this.userService.findUser( username );
  }

  addUser( username ){
    console.log(username);
  }

  filterItems(searchTerm){
 
        return this.userService.usersList.filter((item) => {
            return item.uname.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });     
 
    }

  // findUser( filterTerm ) {
  //     console.log( filterTerm );
  //     if( filterTerm != "" ){
  //       this.foundUser = true;
  //       this.foundUser = this.filterItems( filterTerm );
  //     } else {
  //       this.foundUser = false;
  //     }
  //     console.log(this.foundUser);

  // }

  userDetails( username ) {
    console.log( username );
    this.navCtrl.push( this.userProfile, {
      username: username,
      type: "indi"
    } );
  }

  fetch(){
    this.userService.getAllUsersList();
  }

  ionViewDidLoad() {
    console.log('Hello FindUserPage Page');
  }

}

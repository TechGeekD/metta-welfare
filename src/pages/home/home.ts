import { Component, Injectable } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { UserService } from '../../providers/user-service';

import { RegisterPage } from '../register/register';
import { UserPage } from '../user/user';

@Injectable()

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  username: any;
  password: any;
  logStatus: any = "hello world";
  time: any;

  registerPage = RegisterPage;
  userPage = UserPage;

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public storage: Storage, public userService: UserService) {
    this.menuCtrl.enable(false);
  }

  login() {
      this.logStatus = this.userService.login(this.username, this.password);
      console.log("logStatus 1 " + this.logStatus);
      this.time = setInterval(() => {
        if (this.logStatus == true) {
          clearInterval(this.time);
        } else if (this.logStatus == "error") {
          clearInterval(this.time);
        }
        this.getLogStatus();
      }, 50);
  }

  tapEvent(e){
    if( e != "re" ){
        if( this.userService.server == 'http://192.168.1.3:90' ) {

          this.userService.server = 'http://192.168.43.79:90';

        } else if ( this.userService.server == 'http://192.168.43.79:90' )  {

          this.userService.server = 'https://evening-waters-53433.herokuapp.com';

        } else if ( this.userService.server == 'https://evening-waters-53433.herokuapp.com' ) {

          this.userService.server = 'http://localhost:90'; 

        } else if ( this.userService.server == 'http://localhost:90' ) {

          this.userService.server = 'http://192.168.1.3:90';

        }

        console.log(this.userService.server);
        // this.userService.presentToast( "serverSwitch" );
        // if(this.userService.logStatus == 'retry'){
        //   this.userService.loggedInAlready();
        // }
    } else {
      this.userService.loggedInAlready();
    }

  }

  getLogStatus() {
    this.logStatus = this.userService.getLogStatus();
    console.log("logStatus 2 " + this.logStatus);
    if (this.logStatus == true) {
      console.log("In goToUser");
      this.goToUser();
    } else {
      console.log("In goToUser ERROR");
    }
  }

  goToUser() {
    this.navCtrl.setRoot(this.userPage, {
      player: this.userService.getPlayer()
    }).catch(error => {
      console.log(error);
    });
  }

  onPageWillLeave() {
    this.menuCtrl.enable(true);
  }

}
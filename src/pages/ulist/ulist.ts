import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { Headers, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()

@Component({
  selector: 'page-ulist',
  templateUrl: 'ulist.html'
})

export class UlistPage {
  @ViewChild(Content) content: Content;

  http;
  nav;

  arrayOfKeys: any;
  player: any;
  url: any;
  see: any = [{
    uname: undefined,
    email: undefined,
    phone: undefined,
    statusx: undefined
  }];
  seeUserStatus: boolean = true;
  array: any = [{
    uname: undefined,
    email: undefined,
    phone: undefined,
    statusx: undefined
  }];

  constructor(public navCtrl: NavController, navParams: NavParams, httpService: Http) {
    this.http = httpService;
    this.nav = navCtrl;
    this.player = navParams.get("player");
    this.fetch();
  }

  private headers = new Headers({
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
    'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
    'Access-Control-Max-Age': '86400'
  });

  fetch() {

    //uncomment url on building app
    // this.url = 'http://192.168.1.3:90/users'; 
    // this.url = 'http://192.168.43.79:90/users';
    this.url = 'https://evening-waters-53433.herokuapp.com/users' ;
    // this.url = 'http://localhost:90/users';
    console.log("REST API URL: " + this.url);

    this.http.get(this.url, { headers: this.headers })
      .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
        if (data) {
          this.player = data;
          this.convert();
          console.log("player1:");
          console.log(this.player);
        } else {
          console.log("NO DATA FETCHED !!");
        }
      });
  }

  convert() {
    for (var x in this.player) {
      this.array = this.player;
    }
    console.log("array1:");
    console.log(this.array);
    console.log("X1:");
    console.log(x);
  }

  seeUser(player: any) {
    console.log("player2:");
    console.log(player);
    console.log("seeUserStatus1:");
    console.log(this.seeUserStatus);
    if (player != false) {
      if (this.see.uname == player.uname && this.seeUserStatus == true) {
        this.seeUserStatus = false;
      } else {
        console.log("player3:");
        console.log(player);
        for (var x in player) {
          this.see = player;
        }
        console.log("seeUserStatus2:");
        this.seeUserStatus = true;
        console.log("player4:");
        console.log(player);
        console.log("X2:");
        console.log(x);
      }
    } else {
      this.seeUserStatus = false;
      console.log("seeUserStatus2:");
      console.log(this.seeUserStatus);
    }
  }

  scroll(player: any) {
    this.seeUser(player);
    setTimeout(() => {
      let element = document.getElementById(player.uname);
      this.content.scrollTo(0, element.offsetTop - 100, 800);
    }, 0);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  ionViewDidLoad() {
    console.log('Hello UlistPage Page');
  }

}

// export class userdatax {

//   constructor(public uname?: string,
//     public fullname?: string,
//     public passwordx?: string,
//     public email?: string,
//     public phone?: number,
//     public statusx?: string,
//     public error?: string) { }
// }

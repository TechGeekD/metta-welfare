import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import { /*File,*/ Camera /*, Transfer*/ } from '@ionic-native/camera';
// import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';
import { AndroidFingerprintAuth, AFAAuthOptions } from '@ionic-native/android-fingerprint-auth';
import { Storage } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class UserService {

  http;
  loader: any;
  json: any;
  url: any;
  server: any;
  time: any;
  img: any;
  logStatus: any = false;
  regStatus: any = undefined;
  deleteStatus: any = false;
  updateStatus: any = false;

  public base64Image: string;

  username: any;
  password: any;
  foundUser: any;
  interest: any;
  interestAll: any;
  player: userdatax;
  usersList: any;
  userProfile: any;
  scanData: any;
  event_leader: any = false;
  groupsList: any;
  notify: any = false;
  groupMember: any = false;
  groupInfo: any;
  groupEventsList: any;
  userExist: any;
  eventDetails: any;
  eventReview: any;
  eventMedia: any = false;
  eventParticipant: any;
  myEvents: any;
  reviews: any;
  attachReq: any;
  allFingerToken: any;
  attendance: any;

  constructor(public httpService: Http, private androidFingerprintAuth: AndroidFingerprintAuth, public storage: Storage, private fileChooser: FileChooser, private file: File, private screenOrientation: ScreenOrientation, private barcodeScanner: BarcodeScanner, private camera: Camera, private toastCtrl: ToastController, private alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    console.log('Hello UserService Provider');
    this.http = httpService;
    this.setURL();
  }

  setURL(){
      // this.storage.get('server').then((val) =>{
      //   this.server = val;
      // });
      // this.server = 'http://192.168.1.3:90';
      // this.server = 'http://192.168.43.79:90';
      this.server = 'https://evening-waters-53433.herokuapp.com';
      // this.server = 'http://localhost:90';
      console.log("SERVER URL SETUP DONE !");
  }


  // Header for httpServices
    private headers = new Headers({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
      // 'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
      // 'Access-Control-Max-Age': '86400'
    });


  // GET & SET user information
    setPlayer(player: any) {
      this.player = player;
      console.log(this.player);
    }

    getPlayer() {
      return this.player;
    }

    getUserProfile( username, type ){
      this.url = this.server + '/users/profile';
      console.log("REST API URL: " + this.url);
      
      this.json = JSON.stringify({ uname: username, type: type });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
                this.userProfile = data;
            }
          });
      
    }

    setUserProfile(){
      this.json = JSON.stringify({ unamex: this.player.uname, uname: this.player.uname, fullname: this.player.fullname, passwordx: this.player.passwordx, email: this.player.email, phone: this.player.phone, statusx: this.player.statusx });
      this.url = this.server + "/users";

      this.http.put(this.url, this.json, { headers: this.headers }).subscribe(data => {
        this.json = data._body;
        console.log(data);
        this.presentToast("updated");
      }, error => {
        console.log("Oooops!" + error);
      });
    }

    getGroupList(){
      this.url = this.server + '/groups/get';
      console.log("REST API URL: " + this.url);

      this.json = JSON.stringify({ username: this.username });

      this.http.post(this.url, this.json, { headers: this.headers })
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
          console.log(data);
          if (data) {
            this.groupsList = data;
            console.log(this.groupsList);
          } else {
            console.log("NO GroupsDATA FETCHED !!");
          }
        });
        
    }

    findUser( username ){
      this.url = this.server + '/users/find';
      console.log("REST API URL: " + this.url);
      let val = username.target.value;

      this.json = JSON.stringify({ uname: val });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              if( data.err == 'notfound'){
                this.foundUser = 404
              } else {
                this.foundUser = data;
              }
              console.log(val);
            }
          });
          
    }

    getAllUsersList(){
      //uncomment url on building app
      this.url = this.server + '/users';
      console.log("REST API URL: " + this.url);

      this.http.get(this.url, { headers: this.headers })
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
          if (data) {
            this.usersList = data;
            console.log("player1:");
            console.log(this.usersList);
          } else {
            console.log("NO UserDATA FETCHED !!");
          }
        });
        
    }

    setEventLeader( ev_id, lead_id ){
      this.url = this.server + '/events/leader';
      this.json = JSON.stringify({ ev_id: ev_id, lead_id: lead_id });
      console.log("REST API URL: " + this.url);
        
      this.http.put(this.url, this.json, { headers: this.headers })
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            if(data){
              console.log(data);
              if(data == "EventLeaderSet") {
                this.presentToast("eventLead");
              } else {
                this.presentToast("eventLeadError");
              }
            } 
        });
    }

    getEventLeader( ev_id, leader_id, type ){
      this.url = this.server + '/events/get/leader';
      console.log("REST API URL: " + this.url);

      this.json = JSON.stringify({ ev_id: ev_id, leader_id: leader_id, type });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            if (data) {
                this.event_leader = data;
                console.log( this.event_leader );
            }
          });
    }

    setEventStatus( ev_id, status ) {
      this.url = this.server + '/events/status';
      this.json = JSON.stringify({ ev_id: ev_id, status: status });
      console.log("REST API URL: " + this.url);
        
      this.http.put(this.url, this.json, { headers: this.headers })
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            if(data){
              console.log(data);
              if(data == "EventStatusSet") {
                this.presentToast("eventStatus");
              } else {
                this.presentToast("eventStatusError");
              }
            } 
        });
    }

    getNotify(){
      this.url = this.server + '/notify';

      this.storage.get('MaxNotifyId').then((val) => {
        
        if( val == null ){
          this.json = JSON.stringify({ user_id: this.player.user_id, maxNotifyId: 0 });
        } else {
          this.json = JSON.stringify({ user_id: this.player.user_id, maxNotifyId: val });
        }
        console.log("REST API URL: " + this.url);
        
        this.http.post(this.url, this.json, { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if(data[0] != undefined){
              if (data[0].nid) {
                this.notify = []
                data.forEach(element => {
                  if ( element.type == 'event'  ){
                    this.notify.push(element);
                  } else if ( element.type == 'request' && element.user_id == this.player.user_id ){
                    this.notify.push(element);
                  }
                });
                // if( val != null ){
                  // var max = 0;
                  // this.notify.forEach(element => {
                  //   if( element.nid > max ){
                  //     max = element.nid;
                  //   }
                  // });
                  // this.storage.set('MaxNotifyId',max);                  
                  // console.log("MAX: "+max);
                // } else if( val == null ){
                  // this.storage.set('MaxNotifyId',0);
                // }  
              } else {
                this.notify = false;
                console.log("User Notification: "+this.notify);
              }
            } else {
              this.notify = false;
              console.log("Database Notification: "+this.notify);
            }
          });

      }).catch(this.handleError);
      
    }

    setMaxNotifyId( maxNotifyId ){
      this.url = this.server + '/notify/setMax';
      this.json = JSON.stringify({ user_id: this.player.user_id, maxNotifyId: maxNotifyId });
      console.log("REST API URL: " + this.url);
        
      this.http.post(this.url, this.json, { headers: this.headers })
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if(data){
              console.log("maxNotifyId Set In DB");
            }
        });
        
    }

    getEventDetails( ev_id ){

      this.attendance = false;

      this.url = this.server + '/events/details';

      this.json =  JSON.stringify({ ev_id: ev_id });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.eventDetails = data[0][0];
              data[1].forEach(element => {
                console.log(element.user_id +' & '+ this.player.user_id);
                if( this.player.user_id = element.user_id ){
                  this.attendance = true;
                }
                console.log(this.attendance);
              });
              let monthNames = [
              "Jan", "Feb", "Mar",
              "Apr", "May", "Jun", "Jul",
              "Aug", "Sep", "Oct",
              "Nov", "Dec"
            ];
              let date = new Date(this.eventDetails.event_date);
              let day = date.getDate();
              let monthIndex = date.getMonth();
              let year = date.getFullYear();
              this.eventDetails.event_date = day + ' ' + monthNames[monthIndex] + ' ' + year;
              // this.getPic( ev_id );
            }
          });
          
          this.getEventReviews( ev_id );
    }

    getEventReviews( ev_id ){
      this.url = this.server + '/review/get';

      this.json =  JSON.stringify({ ev_id: ev_id });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.reviews = data;
            }
          });
        this.getEventMedia( ev_id, "event" );

    }

    getEventParticipant( ev_id ){
      this.url = this.server + '/events/participant';

      this.json =  JSON.stringify({ ev_id: ev_id });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.eventParticipant = data;
            }
          });
      
    }

    groupDetails( group_name ){

      this.url = this.server + '/groups/details';
      console.log("REST API URL: " + this.url);

      this.json = JSON.stringify({ group_name: group_name });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.groupInfo = data;
              console.log(this.groupInfo[0].member);
            }
          });
              
    }

    groupsEvents( group_name ){
      this.url = this.server + '/groups/events';
      console.log("REST API URL: " + this.url);

      this.json = JSON.stringify({ group_name: group_name });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.groupEventsList = data;
            }
          });
          
    }

    takeAttendance( ev_id, type ){
      console.log(ev_id,type);
        if( type == "barcode" ){
          console.log("SCAN BARCODE");
          this.barcodeScanner.scan().then((barcodeData) => {
                // Success! Barcode data is here
                console.log(barcodeData);
                if( barcodeData ){
                  this.scanData = barcodeData.text;
                  // let prompt = this.alertCtrl.create({
                  //   title: 'BARCODE SCANNER',
                  //   message: "Scanned Barcode information",
                  //   inputs: [
                  //     {
                  //       name: 'barcode',
                  //       placeholder: 'Barcode Info',
                  //       value: this.scanData
                  //     }
                  //   ],
                  //   buttons: [
                  //     {
                  //       text: 'Cancel',
                  //       handler: data => {
                  //         console.log('Cancel clicked');
                  //       }
                  //     },
                  //     {
                  //       text: 'Save',
                  //       handler: data => {
                  //         console.log('Saved clicked',data);
                  //       }
                  //     }
                  //   ]
                  // });
                  // prompt.present();
                } else {
                  this.scanData = "Cancled";
                  console.log("Cancled")
                }
            }, (err) => {
                // An error occurred
                console.log(err);
            });
        }
    }

    setAttendance( ev_id ){

      this.url = this.server + '/attendance/set';
      console.log("REST API URL: " + this.url);
      this.json = JSON.stringify({ ev_id: ev_id, scanData: this.scanData, status: "present" });

      this.http.post(this.url, this.json, { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {            
            if (!data[0].err) {
              console.log(data);
              this.presentToast("attend");
            } else {
              this.presentToast("attend_err");
            }
          });          
    }

    getAllInterest(){
      this.url = this.server + '/events/interest/get';
      console.log("REST API URL: " + this.url);

      this.http.get(this.url, { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.interestAll = data;
            }
          });
    }

    getUserInterest(){
      this.url = this.server + '/events/interest/get';
      console.log("REST API URL: " + this.url);

      this.json = JSON.stringify({ user_id: this.player.user_id });

      this.http.post(this.url, this.json, { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.interest = data;
            }
          });
    }

    setInterest( interest ){
      this.url = this.server + '/events/interest/set';
      console.log("REST API URL: " + this.url);
      this.json = JSON.stringify({ interest: interest, user_id: this.player.user_id });

      this.http.post(this.url, this.json, { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {            
            if (data) {
              console.log(data);
            }
          });
    }    

  // Media Management Function
    managePic( event_name, type ){
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      this.url = this.server + '/media/image';
      // let options: CaptureImageOptions = { limit: 1 };
      // this.mediaCapture.captureImage(options)
      //   .then(
      //     (data: MediaFile[]) => console.log(data),
      //     (err: CaptureError) => console.error(err)
      //   );

      if( type == "lib" ){
        var options = {
          quality: 100,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
          destinationType: this.camera.DestinationType.DATA_URL,
          saveToPhotoAlbum: false,
          correctOrientation: true,
          allowEdit: true
        };
      } else {
         var options = {
          quality: 100,
          sourceType: this.camera.PictureSourceType.CAMERA,
          destinationType: this.camera.DestinationType.DATA_URL,
          saveToPhotoAlbum: false,
          correctOrientation: true,
          allowEdit: true
        };
      }
      
      this.camera.getPicture(options).then((imageData) => {
            // imageData is a base64 encoded string
              this.base64Image = "data:image/jpeg;base64," + imageData;

              this.json = JSON.stringify({ file: this.base64Image, username: this.username, event_name: event_name });
              // this.screenOrientation.unlock();
              this.http.post(this.url, this.json, { headers : 'multipart/form-data' }).subscribe(data => {
                
                this.json = data;
                this.getPic( event_name );

              }, error => {
                console.log("Image Process Error: " + error);
              });
              console.log(this.base64Image);
          }, (err) => {
              console.log(err);
          });
    }

    setEventMedia( ev_id, event_name, type ){
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      this.url = this.server + '/media/insert';

      var options = {
          quality: 100,
          sourceType: this.camera.PictureSourceType.CAMERA,
          destinationType: this.camera.DestinationType.DATA_URL,
          saveToPhotoAlbum: false,
          correctOrientation: true
        };

      this.camera.getPicture(options).then((imageData) => {
            // imageData is a base64 encoded string
              this.base64Image = "data:image/jpeg;base64," + imageData;

              this.json = JSON.stringify({ file: this.base64Image, med_type: "image", category: "event", ev_id: ev_id, event_name: event_name });
              // this.screenOrientation.unlock();
              this.http.post(this.url, this.json, { headers : 'multipart/form-data' }).subscribe(data => {
                
                this.json = data;
                console.log("post: ");
                console.log(data);
                this.getEventMedia( ev_id, type );

              }, error => {
                console.log("Media Process Error: " + error);
              });
              console.log(this.base64Image);
          }, (err) => {
              console.log(err);
          });
    }

    getEventMedia( ev_id, type ) {
      this.url = this.server + '/media/get';

      this.json =  JSON.stringify({ ev_id: ev_id, type: type });

      this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {
              this.eventMedia = data;
            }
          });

      this.getEventParticipant( ev_id );
    }

    getPic( event_name ){
      this.url = this.server + '/media/image/get';

      this.json = JSON.stringify({ username: this.username, event_name: event_name });
      
      this.http.post(this.url, this.json, { headers : this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {

        console.log(data);
        this.img = data[0];
        console.log("ImageData Fetched: ");
        console.log(this.img);
      }, error => {
          console.log("Image get Error: " + error);
        });
        
    }


  // Login User Function & Validation
    loggedInAlready(){
      this.storage.get('username').then((val) => {
          
          this.username = val;
          
          this.storage.get('password').then((val) => {
            
            this.password = val;
            this.login( this.username, this.password );

            // setTimeout(()=>{
            //   if( this.logStatus == true ){
                
            //   } else {
            //     console.log("loggedInAlready Error !");
            //   }
            // },1000);

          });
      });
    }

    //WORKING BUT NEED LOGIC FIX
    fpAuth( authType ){

      this.androidFingerprintAuth.isAvailable()
          .then((result)=> {
            if(result.isAvailable){
              // it is available
              var config: AFAAuthOptions = {
                  clientId: "Metta_Welfare",
                  username: this.player.uname,
                  password: this.player.passwordx,
              };

              if( authType == 'encrypt' ){
                  this.androidFingerprintAuth.encrypt(config)
                    .then(result => {
                      if (result.withFingerprint) {
                          console.log("Successfully encrypted credentials.");
                          console.log("Encrypted credentials: " + result.token);
                          
                          //http set token to database
                          this.json = JSON.stringify({ token: result.token, user_id: this.player.user_id });
                          this.url = this.server + "/users/fpAuthToken/set";

                          this.http.post(this.url, this.json, { headers: this.headers }).subscribe(data => {
                            this.json = data._body;
                            console.log(data);
                            this.presentToast("finger");
                          }, error => {
                            console.log("Oooops!" + error);
                          });

                      } else if (result.withBackup) {
                        console.log('Successfully authenticated with backup password!');
                      } else console.log('Didn\'t authenticate!');
                    })
                    .catch(error => {
                      if (error === "Cancelled") {
                        console.log("Fingerprint authentication cancelled");
                      } else console.error(error)
                    });
              } else if( authType == 'decrypt') {

                  //http get token from database
                  this.url = this.server + "/users/fpAuthToken/get";

                  this.http.get(this.url, { headers: this.headers })
                          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
                  this.allFingerToken = data;
                  console.log(this.allFingerToken);
                  // this.allFingerToken.forEach(token => {
                  
                    this.androidFingerprintAuth.decrypt({ clientId: "Metta_Welfare", username: config.username, token: data[0].fptoken  })
                      .then(result => {
                        console.log(result);
                        if (result.withFingerprint) {
                            console.log("Successfully Decrypted credentials.");
                            //set global true value

                        } else if (result.withBackup) {
                          console.log('Successfully authenticated with backup password!');
                        } else console.log('Didn\'t authenticate!');
                      })
                      .catch(error => {
                        if (error === "Cancelled") {
                          console.log("Fingerprint authentication cancelled");
                        } else console.error(error)
                      });

                    // });
                  }, error => {
                    console.log("Oooops!" + error);
                  });

                  
              } else {
                  this.androidFingerprintAuth.delete({ clientId: config.clientId, username: config.username })
              }
            } else {
              // fingerprint auth isn't available
              console.log("Fingerprint Auth is not available");
            }
          })
          .catch(error => console.error(error));
    }

    login(username: any, password: any) {

      this.url = this.server + '/users/login';
      console.log("REST API URL: " + this.url);
      this.presentLoading(true);
      if (username && password) {
        console.log("Password: " + password);
        this.json = JSON.stringify({ uname: username, passwordx: password });
        this.http.post(this.url, this.json , { headers: this.headers })
          .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
            console.log(data);
            if (data) {

              this.player = data;
              console.log("Login Datax:  OK");
              console.log("Useruname:"+this.player.uname);
              console.log("Password:"+this.player.passwordx);

              if (this.player.passwordx == password && !this.player.error) {
                console.log("Login Successx:  OK");
                this.presentToast("login");
                this.username = this.player.uname;
                this.password = this.player.passwordx;
                this.logStatus = true;
                this.storage.set('logStatus', this.logStatus);
                this.storage.set('username', this.username);
                this.storage.set('password', this.password);
              } else {
                console.log(this.player.error);
                this.presentToast("upwrong");
                this.logStatus = "error";
              }
            } else {
              console.log("Error login !!!");
              this.presentToast("unotfound");
              this.logStatus = "error";
            }
          });
      } else {
        console.log("Provide Data !!!")
        this.presentToast("upmiss");
        this.logStatus = "error";
      }
      
    }

    getLogStatus() {
      console.log(this.logStatus);
      if (this.logStatus == true || this.logStatus == false || this.logStatus == "error") {
        return this.logStatus;
      } else {
        return "Internal Error";
      }
    }


  // Registeration Function & Validation
    register() {

      this.presentLoading(true);
      this.json = JSON.stringify({ uname: this.player.uname, fullname: this.player.fullname, passwordx: this.player.passwordx,email: this.player.email, phone: this.player.phone, statusx: this.player.statusx });
      console.log("register");
      console.log(this.json);
      this.url = this.server + '/users';

      this.http.post(this.url, this.json, { headers: this.headers }).subscribe(data => {
        this.json = data;
        console.log("post: ");
        console.log(data);
        this.regStatus = true;
      }, error => {
        this.presentLoading(false);
        this.regStatus = false;
        console.log("Registeration Error: " + error);
      });

      this.presentToast("register");
      setTimeout(() =>{
        this.regStatus = undefined;
      } ,1000);
    }

    checkUser(username: any) {
      this.regStatus = undefined;
      this.url = this.server + '/users/profile';
      this.json = JSON.stringify({ uname: username, type: 'indi' });
      console.log("REST API URL: "+this.url);
      this.http.post(this.url, this.json, { headers: this.headers })
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
          console.log("Condition: ");
          if (data[0]) {
            console.log("false");
            this.presentToast("userexist");
          } else {
            console.log("true");
            this.register();
          }
        });
      
    }

    getRegStatus() {
      if (this.regStatus == true || this.regStatus == false) {
        return this.regStatus;
      } else {
        return "Internal Error";
      }
    }

    createGroup( groupName, type, username = null ){

      if( type == 'createGroup' ){
        this.groupMember.push({ uname: this.username, mem_type: "leader" },{ group_name: groupName });
        this.url = this.server + '/groups';
      } else if( type == 'updateGroup' ) {
        this.groupMember = { uname: username, group_name: groupName, type: 'addMember', mem_type: "member" };
        this.url = this.server + '/groups/add';
      }
      // var jsonGroup: any = false;
      // var i = 2;
      // this.groupMember.forEach(element => {
        // if( jsonGroup != false ){
        //   var data = '{ "uname'+i+'": "'+element.uname+'" }'; 
        //   data = JSON.parse(data);
        //   jsonGroup.push(data);
        //   i += 1;
        // } else {
        //   jsonGroup = [{ 'uname1': element.uname}]
        // }
      // });
      this.json = JSON.stringify(this.groupMember);
      console.log(this.url);
        this.http.post(this.url, this.json, { headers: this.headers }).subscribe(data => {
          console.log(data);
          if(data._body == 'err'){
            this.presentToast('groupExist');
          }
        }, error => {
          console.log("Create Group Error! " + error);
        });
        
    }

    attachRequest(){
      this.fileChooser.open()
          .then(uri => {
            console.log(uri);
            
            this.file.resolveLocalFilesystemUrl(uri).then(path =>{
              console.log(path);
              let url = path.nativeURL.substr(0,path.nativeURL.lastIndexOf('/')+1);
              console.log(url);
              this.file.readAsDataURL(url, path.name).then(data =>{
                console.log(data);
              }).catch(e => console.log(e));

              this.file.resolveDirectoryUrl(url).then(directoryEntry => {
                console.log(directoryEntry)
                // this.file.getFile(directoryEntry, path.name, { create: true }).then(data =>{

                // }).catch(e => console.log(e));
              }).catch(e => console.log(e));
            }).catch(e => console.log(e));

          })
          .catch(e => console.log(e));
    }


  // Delete, Add User & Event
    deleteUsersEvent( event_name ){
      this.presentConfirm("deleteEvent");
      this.time = setInterval(() => {
        if(this.deleteStatus){
          this.presentLoading(true);
          console.log('YES clicked');
          this.url = this.server + '/events/delete';
          let options = new RequestOptions({
            headers: this.headers,
            uname: this.player.uname,
            event_name: event_name
          });

          this.http.delete(this.url, options).subscribe(data => {
            this.json = data;
            this.deleteStatus = true;
            console.log(data);
            this.presentToast("eventDelete");
            return true;
          }, error => {
            this.deleteStatus = "err"
            this.presentToast("eventDeleteError");
            console.log("deleteUsersEvent Error! " + error);
          });

        } else {
          console.log("password Wrong!")
        }
        if (this.deleteStatus == true || this.deleteStatus == "err") {
          clearInterval(this.time);
          this.deleteStatus = false;
        }
      }, 3000);
      
    }

    deleteEvent( event_name ){
      this.presentConfirm("deleteEvent");
      this.time = setInterval(() => {
        if(this.deleteStatus){
          this.presentLoading(true);
          console.log('YES clicked');
          this.url = this.server + '/events/main_delete';
          let options = new RequestOptions({
            headers: this.headers,
            event_name: event_name
          });

          this.http.delete(this.url, options).subscribe(data => {
            this.json = data;
            this.deleteStatus = true;
            console.log(data);
            this.presentToast("eventDelete");
            return true;
          }, error => {
            this.deleteStatus = "err";
            this.presentToast("eventDeleteError");
            console.log("deleteEvent Error! " + error);
          });
        
        } else {
          console.log("password Wrong!")
        }
        if (this.deleteStatus == true) {
          clearInterval(this.time);
          this.deleteStatus = false;
        }
      }, 3000);
      
    }

    addUser2Group( username ){
      console.log("Add User To Group:"+username);
      if( this.groupMember != false ){
        this.userExist = false;
        this.groupMember.forEach(element => {
          if( element.uname == username ){
            console.log("User Already Exist in Group !");
            this.userExist = true;
          }
        });
        if( this.userExist == false ){
          this.groupMember.push({ uname: username, mem_type: "member" });
        }

      } else {
        this.groupMember = [{ uname: username, mem_type: "member" }];
      }
      console.log(this.groupMember);
    }

    joinRequest( ev_id, group_id ) {
      this.url = this.server + '/events/request';
      if( group_id != null ){
        this.json = JSON.stringify({ ev_id: ev_id, group_id: group_id, type: "group" });
      } else {
        this.json = JSON.stringify({ ev_id: ev_id, user_id: this.player.user_id, type: "indi" });
      }

      console.log(this.url);
      this.http.post(this.url, this.json, { headers: this.headers }).subscribe(data => {
        if(data){
          console.log(data);
          this.presentToast("reqJoin");
        }
      }, error => {
        console.log("Event Join Request Error! " + error);
      });
    }

    getEventReqStatus( group_id ) {
      this.url = this.server + '/events/request/get';
      if( group_id != null ){
        this.json = JSON.stringify({ group_id: group_id });
      } else {
        this.json = JSON.stringify({ user_id: this.player.user_id });
      }

      console.log(this.url);
      this.http.post(this.url, this.json, { headers: this.headers })
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
          if(data){
            this.myEvents = data;
            console.log(this.myEvents);
          }
      }, error => {
        console.log("Event Join Request Error! " + error);
      });
    }

    removeGroup( group_name ){
      this.presentConfirm("deleteEvent");
      this.time = setInterval(() => {
        if(this.deleteStatus){
          this.presentLoading(true);
          console.log('YES clicked');
          //uncomment url on building app
          this.url = this.server + '/groups/delete';
          let options = new RequestOptions({
            headers: this.headers,
            group_name: group_name
          });

          this.http.delete(this.url, options).subscribe(data => {
            this.json = data;
            this.deleteStatus = true;
            console.log(data);
            this.presentToast("eventDelete");
            return true;
          }, error => {
            this.deleteStatus = "err";
            this.presentToast("eventDeleteError");
            console.log("Group Delete Error!" + error);
          });
        
        } else {
          console.log("password Wrong!")
        }
        if (this.deleteStatus == true || this.deleteStatus == "err" ) {
          clearInterval(this.time);
          this.deleteStatus = false;
        }
      }, 3000);
      
    }

    removeGroupMember( member, group_name ){
      this.presentConfirm("deleteEvent");
      this.time = setInterval(() => {
        if(this.deleteStatus){
          this.presentLoading(true);
          console.log('YES clicked');
          this.url = this.server + '/groups/member/delete';
          let options = new RequestOptions({
            headers: this.headers,
            member: member,
            group_name: group_name
            
          });

          this.http.delete(this.url, options).subscribe(data => {
            this.json = data;
            this.deleteStatus = true;
            console.log(data);
            this.presentToast("eventDelete");
            return true;
          }, error => {
            this.deleteStatus = "err";
            this.presentToast("eventDeleteError");
            console.log("GroupMember Delete Error!" + error);
          });
        
        } else {
          console.log("password Wrong!")
        }
        if (this.deleteStatus == true || this.deleteStatus == "err") {
          clearInterval(this.time);
          this.deleteStatus = false;
        }
      }, 2000);
      
    }

    removeGroupEvent( event_name, group_name ){
      this.presentConfirm("deleteEvent");
      this.time = setInterval(() => {
        if(this.deleteStatus){
          this.presentLoading(true);
          console.log('YES clicked');
          this.url = this.server + '/groups/event/delete';
          let options = new RequestOptions({
            headers: this.headers,
            event_name: event_name,
            group_name: group_name
          });

          this.http.delete(this.url, options).subscribe(data => {
            this.json = data._body;
            this.deleteStatus = true;
            console.log(data);
            this.presentToast("eventDelete");
            return true;
          }, error => {
            this.deleteStatus = "err";
            this.presentToast("eventDeleteError");
            console.log("GroupEvent Delete Error!" + error);
          });
        
        } else {
          this.deleteStatus == "err";
          console.log("password Wrong!")
        }
        if (this.deleteStatus == true || this.deleteStatus == "err") {
          clearInterval(this.time);
          this.deleteStatus = false;
        }
      }, 2000);
      
    }

    addReview( review, event_name ) {
      this.url = this.server + '/review';
      this.json = JSON.stringify({ event_name: event_name, review: review, user_id: this.player.user_id });
      console.log(this.url);
      this.http.post(this.url, this.json, { headers: this.headers }).subscribe(data => {
        console.log(data);
        if(data){
          this.eventReview = data;
        }
      }, error => {
        console.log("Add Review Error! " + error);
      });
      
    }

    submitRequest( title, desc, cate ){
      console.log("Title: "+title+" Desc: "+desc);
      this.url = this.server + '/request';
      this.json = JSON.stringify({ req_title: title, req_desc: desc, req_cate: cate, user_id: this.player.user_id });
      console.log(this.url);
      this.http.post(this.url, this.json, { headers: this.headers }).subscribe(data => {
        console.log(data._body);
        if(data._body == 'RequestAdded'){
          // this.eventReview = data;
          this.presentToast('reqSubmit');
        } else {
          this.presentToast('reqSubmit_err2');
        }
      }, error => {
        console.log("Add Review Error! " + error);
      });
      
    }


  // System Notification, Loading & Confirm Dialog Box
    presentConfirm(testCode: any) {
      if (testCode == "deactive") {
        let alert = this.alertCtrl.create({
          title: 'Confirm Deactivation',
          message: 'Enter Password to Confirm Deactivation',
          inputs: [
            {
              name: 'password',
              placeholder: 'Enter Password'
            },
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: data => {
                console.log('Cancel clicked');
                this.logStatus = "error";
                setTimeout(() => this.logStatus = true, 500)
              }
            },
            {
              text: 'YES',
              handler: data => {
                if (this.password == data.password) {
                  this.presentLoading(true);
                  console.log('YES clicked');
                  this.url = this.server + '/users';
                  let options = new RequestOptions({
                    headers: this.headers,
                    uname: this.player.uname
                  });

                  this.http.delete(this.url, options).subscribe(data => {
                    this.json = data;
                    this.logStatus = false;
                    console.log(data);
                  }, error => {
                    console.log("Oooops!" + error);
                  });
                  console.log(testCode);
                  this.presentToast(testCode);
                } else {
                  console.log("Error Deactivation " + this.password + " & " + data.password);
                  this.logStatus = "error";
                  setTimeout(() => this.logStatus = true, 500)
                  this.presentToast("deactpasswordmiss");
                }
                
              }
            }
          ]
        });
        alert.present();
      } else if (testCode == "logout") {
        let alert = this.alertCtrl.create({
          title: 'Confirm Logout',
          message: 'Do you want to Logout of this Account?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
                this.logStatus = "error";
                setTimeout(() => this.logStatus = true, 500)
              }
            },
            {
              text: 'Logout',
              handler: () => {
                this.presentLoading(true);
              // this.player.empno = undefined;
                this.logStatus = false;
                this.storage.remove('username').then(() => {
                  console.log('Username has been removed');
                  
                  this.storage.remove('password').then(() => {
                    console.log('Password has been removed');
                  });

                });
                this.storage.set('logStatus', this.logStatus);
                console.log('YES clicked');
                console.log(testCode);
                this.presentToast("logout");
              }
            }
          ]
        });
        alert.present();
      } else if (testCode == "deleteEvent") {
        let alert = this.alertCtrl.create({
          title: 'Confirm Deletion',
          message: 'Enter Password to Confirm Deletion',
          inputs: [
            {
              name: 'password',
              placeholder: 'Enter Password'
            },
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: data => {
                console.log('Cancel clicked');
                this.deleteStatus = false;
                setTimeout(() => this.logStatus = true, 500)
              }
            },
            {
              text: 'YES',
              handler: data => {
                console.log(this.password +" & "+data.password);
                if (this.password == data.password) {
                  this.deleteStatus = true;
                } else {
                  console.log("Error Deletion " + this.password + " & " + data.password);
                  this.deleteStatus = false;
                  setTimeout(() => this.logStatus = true, 500)
                  this.presentToast("deactpasswordmiss");
                }
              }
            }
          ]
        });
        alert.present();
      } else if (testCode == "updateEvent") {
        let alert = this.alertCtrl.create({
          title: 'Confirm Updation',
          message: 'Enter Password to Confirm Updation',
          inputs: [
            {
              name: 'password',
              placeholder: 'Enter Password'
            },
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: data => {
                console.log('Cancel clicked');
                this.updateStatus = false;
              }
            },
            {
              text: 'YES',
              handler: data => {
                console.log(this.password +" & "+ data.password);
                if (this.password == data.password) {
                  this.updateStatus = true;
                  setTimeout(() => this.updateStatus = false, 5000);
                  console.log('Update True');
                } else {
                  console.log("Error Updation " + this.password + " & " + data.password);
                  this.updateStatus = false;
                  this.presentToast("deactpasswordmiss");
                }
              }
            }
          ]
        });
        alert.present();
      } else {
        let alert = this.alertCtrl.create({
          title: 'Confirm Registeration Details',
          message: 'All Details Are Correct ?',
          buttons: [
            {
              text: 'Edit',
              role: 'cancel',
              handler: () => {
                this.regStatus = false;
                console.log('Cancel clicked'+this.regStatus);
              }
            },
            {
              text: 'Done',
              handler: () => {
                console.log('YES clicked');
                this.checkUser(this.player.uname);
              }
            }
          ]
        });
        alert.present();
      }
    }

    presentLoading(testCode: boolean) {
      if (testCode) {
        this.loader = this.loadingCtrl.create({
          content: "Please wait...",
          duration: 3000,
          dismissOnPageChange: true
        });
        this.loader.present();
      } else {
        this.loader.dismiss();
      }
      console.log(testCode);
    }

    presentToast(testCode: any) {
      console.log(testCode);
      if (testCode == "updated") {
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'User Updated successfully',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();

      } else if (testCode == "deactive") {
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'User Deactivated successfully',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "upwrong") {
        this.presentLoading(false);
        testCode == undefined;
        let toast = this.toastCtrl.create({
          message: 'Username or Password Wrong !!',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();

      } else if (testCode == "unotfound") {
        this.presentLoading(false);
        testCode == undefined;
        let toast = this.toastCtrl.create({
          message: 'User not Found !!',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "upmiss") {
        this.presentLoading(false);
        testCode == undefined;
        let toast = this.toastCtrl.create({
          message: 'Username and/or Password Missig !!',
          duration: 2000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "login") {
        testCode == undefined;
        let toast = this.toastCtrl.create({
          message: 'Logged-in Successfully',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "logout") {
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Logged-out successfully',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "register") {
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'User Registered successfully',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "userexist") {
        testCode = undefined;
        this.regStatus = false;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'User Already Exist',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "groupExist") {
        testCode = undefined;
        this.regStatus = false;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Group Already Exist',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "addMemberError") {
        testCode = undefined;
        this.regStatus = false;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Please Add Atleast 1 member',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "deactpasswordmiss") {
        testCode = undefined;
        this.regStatus = false;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Provide Correct Password to Procced !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventAdd"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'New Event is Added !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventAddError"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Error: Can\'t Add New Event !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventUpdate") {
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Event Details Updated !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventUpdateError"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Error: Can\'t Update Event Details !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventJoin"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Event Joined Successfully !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventLead"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Event Leader Set !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventStatus"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Event Status Set !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventStatusError"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Error: Event Leader Not Set !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventLeadError"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Error: Event Status Not Set !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventJoinError"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Error: Can\'t Join Event !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventDelete"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Deletion Successfull !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "eventDeleteError"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Deletion Error !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "reqJoin"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Request To Join Event Successfull !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "test"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Wait !! I\'m Working On It !',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          let toast = this.toastCtrl.create({
          message: 'phew, Thank God !!!',
          duration: 3000,
          position: 'bottom'
        });
          toast.onDidDismiss(() => {});
          toast.present();
        });

        toast.present();
      } else if (testCode == "serverSwitch"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Server Switched T0: ',
          duration: 1000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          let toast = this.toastCtrl.create({
          message: this.server,
          duration: 2000,
          position: 'bottom'
        });
          toast.onDidDismiss(() => {});
          toast.present();
        });

        toast.present();
      } else if (testCode == "attend"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Attendance: DONE ',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "attend_id"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Attendance: Select Event First ',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "attend_err"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Attendance: User Not Found ',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "reqSubmit"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Request: Submitted Sucessfully ',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "reqSubmit_err"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Request: Please Select Category',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "reqSubmit_err2"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Request: Can\'t Submit Request',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else if (testCode == "addGroupName"){
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Provide Group Name First',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      } else {
        testCode = undefined;
        console.log(testCode);
        let toast = this.toastCtrl.create({
          message: 'Something Went Wrong',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
      }
    }

  // Error Handler
    private handleError(error: Response) {
      console.error(error);
      return Observable.throw(error.json().error || 'Server error');
    }

}

export class userdatax {

  constructor(public user_id?: number,
    public uname?: string,
    public fullname?: string,
    public passwordx?: string,
    public email?: string,
    public phone?: number,
    public statusx?: string,
    public dp?: string,
    public fpAuthToken?: any,
    public error?: string) { }
}

export class RequestOptions {
  constructor(public body?: any) { }
}
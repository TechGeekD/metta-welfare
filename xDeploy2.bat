@echo off

TITLE !IONIC_Deploy by TechGeekD!
cls
echo.
echo 					      !IONIC_Deploy by TechGeekD!
echo.
echo.
echo please check server ip address before procced !!
echo (Press Enter To Continue)
pause > nul

:Msg
cls
echo.
echo 					      !IONIC_Deploy by TechGeekD!
echo.
goto Start:

:Msg1
echo connected To 192.168.1.131:5555
goto Install:
:Msg2
echo connected To 192.168.43.1:5555
goto Install:

:Attempt1
echo Attempt1: Connecting...
adb connect 192.168.1.131 | find "connected to" >nul
if errorlevel 1 (
	goto Attempt2:
) else (
	goto Msg1:
)

:Attempt2
echo Attempt2: Connecting...
adb connect 192.168.43.1  | find "connected to" >nul
if errorlevel 1 (
	goto Attempt1:
) else (
	goto Msg2:
)

:Start
adb devices -l | find "device product:" >nul
if errorlevel 1 (
	goto Attempt1:
) else (
	echo Already Connected!
)

:Install
adb install -r platforms/android/build/outputs/apk/android-debug.apk

adb shell monkey -p com.metta.welfare 1